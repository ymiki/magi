# Try to find GSL, written by Yohei Miki
#
# Result Variables listed in https://cmake.org/cmake/help/v3.2/module/FindGSL.html
# GSL_FOUND                   - True if GSL found on the local system
# GSL_INCLUDE_DIRS            - Location of GSL header files.
# GSL_LIBRARIES	              - The GSL libraries with    GSL CBLAS library.
# GSL_LIBRARIES_WITHOUT_CBLAS - The GSL libraries without GSL CBLAS library.
# GSL_VERSION                 - The version of the discovered GSL install.
#
#
find_program (GSL_CONFIG "gsl-config")
if (GSL_CONFIG)
  # if gsl-config exists, then read the output of gsl-config
  execute_process (COMMAND ${GSL_CONFIG} --prefix             OUTPUT_VARIABLE GSL_INSTALL_DIR OUTPUT_STRIP_TRAILING_WHITESPACE)
  set (GSL_INCLUDE_DIRS "${GSL_INSTALL_DIR}/include")
  execute_process (COMMAND ${GSL_CONFIG} --libs               OUTPUT_VARIABLE GSL_LIBRARIES OUTPUT_STRIP_TRAILING_WHITESPACE)
  execute_process (COMMAND ${GSL_CONFIG} --libs-without-cblas OUTPUT_VARIABLE GSL_LIBRARIES_WITHOUT_CBLAS OUTPUT_STRIP_TRAILING_WHITESPACE)
  execute_process (COMMAND ${GSL_CONFIG} --version            OUTPUT_VARIABLE GSL_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
else (GSL_CONFIG)
  # if gsl-config does not exists, then use the PkgConfig module.
  # the implementation is following https://github.com/Kitware/CMake/blob/master/Modules/FindGSL.cmake
  find_package (PkgConfig)
  pkg_check_modules (GSL REQUIRED QUIET gsl)
  get_filename_component( GSL_ROOT_DIR "${GSL_INCLUDEDIR}" DIRECTORY CACHE)

  message (STATUS "GSL_ROOT_DIR = ${GSL_ROOT_DIR}")
  message (STATUS "GSL_INCLUDEDIR = ${GSL_INCLUDEDIR}")

  find_path (GSL_INCLUDE_DIR
    NAMES gsl/gsl_integration.h
    HINTS ${GSL_ROOT_DIR}/include ${GSL_INCLUDEDIR}
  )
  find_library (GSL_LIBRARY
    NAMES gsl
    HINTS ${GSL_ROOT_DIR}/lib ${GSL_LIBDIR}
    PATH_SUFFIXES Release Debug
  )
  find_library (GSL_CBLAS_LIBRARY
    NAMES gslcblas cblas
    HINTS ${GSL_ROOT_DIR}/lib ${GSL_LIBDIR}
    PATH_SUFFIXES Release Debug
  )

  set (GSL_INCLUDE_DIRS ${GSL_INCLUDE_DIR})
  set (GSL_LIBRARIES ${GSL_LIBRARY} ${GSL_CBLAS_LIBRARY})
  set (GSL_LIBRARIES_WITHOUT_CBLAS ${GSL_LIBRARY})
endif (GSL_CONFIG)

find_package_handle_standard_args (GSL
  FOUND_VAR
    GSL_FOUND
  REQUIRED_VARS
    GSL_INCLUDE_DIRS
    GSL_LIBRARIES
  VERSION_VAR
    GSL_VERSION
  )

mark_as_advanced (GSL_LIBRARIES_WITHOUT_CBLAS GSL_CONFIG)
