# MAGI (version 1.1.1)

* MAny-component Galaxy Initializer
* Developed by
  * Yohei Miki (Information Technology Center, University of Tokyo)
  * Masayuki Umemura (Center for Computational Sciences, University of Tsukuba)
* See Miki & Umemura (2018), MNRAS, 475, 2269 for detail.
* Released under the MIT license, see LICENSE.txt for detail.
* Copyright (c) 2017 Yohei Miki and Masayuki Umemura, All rights reserved.

## How to compile MAGI

0. install libraries
    * (required) install CMake (>= 3.0)
    * (required) install GSL and include GSL_INSTALL_DIR/bin in PATH
    * (optional) install HDF5
    * (optional) download and extract SFMT (SIMD-oriented Fast Mersenne Twister developed by Mutsuo Saito and Makoto Matsumoto)
      * (optional) download and extract SFMT-jump (jump function for SFMT developed by Mutsuo Saito and Makoto Matsumoto)
    * (optional) install Lis (Library of Iterative Solvers for linear systems developed by Nishida et al.)
      * recommended configure option: --enable-omp --enable-fma
      * CAUTION: do not add "--enable-complex"
1. make a build directory and move in

    ```console
    $ mkdir build
    $ cd build
    ```

2. generate Makefile

    ```console
    $ cmake ../
    ```

    * if you want to append options, then type like below (options are listed in below)

      ```console
      $ cmake ../ -DUSE_HDF5=ON -DUSE_TIPSY_FORMAT=OFF -DSFMT_DIR=SFMT_INSTALL_DIR -DSFMT-JUMP_DIR=SFMT_JUMP_INSTALL_DIR
      ```

3. compile the code

    ```console
    $ make
    ```

    * TIPS: below commands are alternative of 1 and 2

      ```console
      $ cmake -H. -Bbuild [option]
      $ cd build
      ```

    * TIPS: below commands may be more friendly for beginners (users can specify options interactively through GUI)

      ```console
      $ cmake -H. -Bbuild
      $ cd build
      $ ccmake ../
      ```

## How to run MAGI

* use the sample shell scripts in the build directory
  * by command-line execution

    ```console
    $ sh/run.sh
    ```

  * by SLURM
    ```console
    $ sbatch sh/slurm.sh
    ```

* results are written in below directories
  * document files are written in doc/
    * doc/unit.txt (description of unit system)
    * doc/filename.summary.txt (summary of the generated system; unit system, # of components, # of disk components, # of particles for each component)
    * doc/filename.info.txt (fundamental information of the generated system)
  * particle data is written in dat/
    * dat/filename.tipsy (for -DUSE_TIPSY_FORMAT=ON; binary format)
    * dat/filename.%d (for -DUSE_GALACTICS_FORMAT=ON; ASCII, index specifies the component)
    * dat/filename.gadget (for -DUSE_GADGET_FORMAT=ON -DUSE_HDF5=OFF; binary format)
    * dat/filename.hdf5 (for -DUSE_GADGET_FORMAT=ON -DUSE_HDF5=ON; ICformat = 3 in GADGET)
    * dat/filename.tmp0.h5 (for -DUSE_HDF5=ON)
  * profile data is written in dat/ (for -DUSE_HDF5=OFF)
    * dat/filename.profile.%d.dat (spherical averaged radial profile of each component in binary format)
    * dat/filename.df.%d.dat (distribution function of spherical components in binary format)
    * dat/filename.diskdat.%d.dat (distribution of disk components in binary format)
    * dat/filename.disk_hor.%d.txt (horizontal profile of disk components in ASCII)
  * profile data is written in dat/ (for -DUSE_HDF5=ON)
    * dat/filename.profile.h5 (spherical averaged radial profile of each component)
    * dat/filename.df.h5 (distribution function of spherical components)
    * dat/filename.disk.h5 (distribution of disk components)
    * dat/filename.property.h5 (characteristic values of each component)
  * miscellaneous files (only for -DUSE_HDF5=ON and used by GOTHIC)
    * dat/filename.cfg.dat
    * dat/filename.run.dat
  * execution log is written in log/

## List of compile options

* USE_TIPSY_FORMAT (On to output particle data in TIPSY format; default is ON)
* USE_GALACTICS_FORMAT (On to output particle data in GalactICS format; default is OFF)
* USE_GADGET_FORMAT (On to output particle data in GADGET format; default is OFF)
* USE_HDF5 (On to build support for HDF5; default is OFF)
* USE_SFMT (On to build support for SFMT; default is ON)
  * SFMT_DIR (Install directory of SFMT; default is SFMT)
  * USE_SFMT19937 (On to use SFMT of period 2^19937 - 1; default is ON)
  * USE_SFMT-JUMP (On to build support for SFMT-jump; default is ON)
    * SFMT-JUMP_DIR (Install directory of SFMT-jump; default is SFMT-Jump)
* USE_LIS (On to build support for Lis; default is OFF)
  * LIS_DIR (Install directory of Lis; default is Lis)
* QUICK_CHECK (On to output partial particle data in ASCII format; default is ON)
  * dat/%s.ascii.txt and dat/ascii.gp (a sample script for gnuplot) are generated.
* USE_GOTHIC (On to use the output for GOTHIC; default is OFF)
  * USE_GOTHIC_BLOCK (On to enable the block time step in GOTHIC; default is ON)
* OMP_THREADS (Specify the number of OpenMP threads if necessary; default is the number of physical CPU cores on the system)
* SET_EXTERNAL_FIELD (On to output radial profiles of each components' potential for N-body simulations; default is OFF)
* ZH78_RETROGRADING (On to produce retrograding particles using Equation (5) of Zang & Hohl (1978); default is OFF)

## List of other options (by editing source codes)

* Adopt similar radial velocity dispersion profile with GalactICS (Kuijken & Dubinski 1995; Widrow et al. 2003)
  * disable ENFORCE_EPICYCLIC_APPROXIMATION defined in src/init/diskDF.h
* Szip compression of HDF5 files
  * enable USE_SZIP_COMPRESSION defined in src/file/io.c for particle data
  * enable USE_SZIP_COMPRESSION defined in src/init/magi.c for utility data
* Skip to generate column density profile
  * disable MAKE_COLUMN_DENSITY_PROFILE defined in src/init/profile.h
* Suppress progress report
  * disable PROGRESS_REPORT_ON defined in src/init/magi.h

## Unit systems in inc/constants.[c h]

* Unit system for particle data is "computational unit" in doc/unit.txt
* Details of the unit system are written in doc/unit.txt (generated by bin/magi)
  * -1: Unity value system    (G = 1)
  *  0: Galactic Nuclei Scale (Msun,  pc, Myr; 10^8 Msun = 1, 100 pc = 1, 1 Myr = 1 in the computational unit)
  *  1: Galactic Scale        (Msun, kpc, Myr; 10^8 Msun = 1, 1 kpc = 1, 100 Myr = 1 in the computational unit)
  *  2: Stellar Scale         (Msun,  AU,  yr; 1 Msun = 1, 1 AU = 1, 1 yr = 1 in the computational unit)
  *  3: G = 1 unit            (Msun, kpc, Myr; 10^8 Msun = 1, 1 kpc = 1, G = 1 in the computational unit)
  *  4: Host Galaxy Scale     (Msun, kpc, Gyr; 10^10 Msun = 1, 10 kpc = 1, 100 Myr = 1 in the computational unit)
  *  5: GalactICS unit system (Msun, kpc, Myr; 1 kpc = 1, 100 km/s = 1, G = 1 in the computational unit)

## How to specify models

* use the sample files included in cfg/.
  * cfg/template/model.cfg and cfg/template/[halo disk].param are template files
  * see Section 2.4 and Listings 1, 2, and 3 in Miki & Umemura (2018), MNRAS, 475, 2269 for details
* location of cfg files
  * files must be located in cfg/
  * MAGI expects \<relative path to the configuration file from ./cfg/\> as input arguments
    * e.g., MAGI reads './cfg/template/model.cfg' when 'template/model.cfg' is passed as input argument (bin/magi -config=template/model.cfg)
* format of cfg files
  * unit system
  * number of components
  * specification of each component
    * \<index\> \<relative path to param files from ./cfg/\> \<specify number of particles (1) or not (0)\> \<specified number of particles (int)\>
* density profile models specified by indices in cfg files (see Miki & Umemura (2018), MNRAS, 475, 2269 and references therein for details of each model)
  * 0: Plummer sphere
  * 1: King sphere (lowered isothermal model)
  * 2: Burkert sphere
  * 3: Hernquist sphere
  * 4: NFW (Navarro--Frenk--White) sphere
  * 5: Moore sphere
  * 6: Einasto sphere
  * 7: Double-power-law sphere
  * 8: Triple-power-law sphere
  * 10: King sphere (in empirical form)
  * 20: Volume-density profile in machine-readable tabular form
  * 21: Surface-density profile in machine-readable tabular form
  * 30: Sersic sphere
  * 31: Double-power-law in surface-density profile
  * 1000: Central black hole
  * -1: Exponential disk
  * -2: Sersic disk
  * -10: Disk in machine-readable tabular form
* format of param files
  * mass of the component in units of astrophysical unit system
  * scale length of the component in units of astrophysical unit system
  * other model specific parameters
  * truncate the envelope of the component (1) or not (0)
    * if truncate the envelope, \<cutoff radius\> \<smoothing scale of the cutoff\>

## File formats of particle data

* GalactICS
  * ASCII (-DUSE_GALACTICS_FORMAT=ON)
    * writeGalactICSFile() in src/file/io.c
  * Tipsy (https://github.com/N-BodyShop/tipsy/wiki/Tipsy)
    * binary (-DUSE_TIPSY_FORMAT=ON)
    * writeTipsyFile() in src/file/io.c
  * GADGET (https://wwwmpa.mpa-garching.mpg.de/gadget/)
    * binary (-DUSE_GADGET_FORMAT=ON -DUSE_HDF5=OFF -->> USE_HDF5_FORMAT is not defined), ICFormat = 2 in GADGET-2
    * HDF5   (-DUSE_GADGET_FORMAT=ON -DUSE_HDF5=ON  -->> USE_HDF5_FORMAT is     defined), ICFormat = 3 in GADGET-2
    * writeGADGETFile() in src/file/io.c
  * GOTHIC
    * binary (-DUSE_HDF5=OFF -->> USE_HDF5_FORMAT is not defined)
    * HDF5   (-DUSE_HDF5=ON  -->> USE_HDF5_FORMAT is     defined)
    * writeTentativeData() in src/file/io.c
    * readTentativeData() in src/file/io.c

## File formats of external potential field

* spherical component(s)
  * ASCII  (-DUSE_GALACTICS_FORMAT=ON -->> binary is set as false)
  * binary (-DUSE_HDF5=OFF -->> USE_HDF5_FORMAT is not defined)
  * HDF5   (-DUSE_HDF5=ON  -->> USE_HDF5_FORMAT is     defined)
  * writeFixedPotentialTable() in src/file/io.c

## How to visualize the particle distribution

* use Matplotlib (in Python 3)

  ```console
  $ python py/tipsy.py filename [rmax] (for TIPSY format)
  $ python py/h5dot.py filename [rmax] (for HDF5 container)
  ```

* use Gnuplot
  * generate Makefile with -DQUICK_CHECK=ON option
  * generate the particle distribution

  ```console
  $ cd dat
  $ gnuplot
  gnuplot> load 'ascii.gp'
  ```

## How to visualize the code

0. install doxygen and graphviz

  ```console
  $ sudo yum install doxygen
  $ sudo yum install graphviz graphviz-gd
  ```

1. generate documents (at the top directory of the distribution)

  ```console
  $ doxygen
  ```

2. see the generated documents

  ```console
  $ firefox html/index.html &
  ```

## Known issue

* compilation with intel compiler and old glibc (< 2.17)
  * undefined references to `clock_gettime`
  * workaround

    ```console
    $ LDFLAGS=-lrt cmake ../ [options]
    $ make
    ```

* compilation with Lis configured with --enable-mpi
  * undefined references to `MPI_*`
  * workaround

    ```console
    $ cmake ../ -DUSE_MPI=ON -DUSE_LIS=ON -DLIS_DIR=/path/to/Lis [options]
    $ make
    ```
