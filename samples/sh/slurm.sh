#!/bin/bash
#SBATCH -J magi               # name of job
#SBATCH -t 00:30:00           # upper limit of elapsed time
#SBATCH -p normal             # partition name
#SBATCH --nodes=1             # number of nodes, set to SLURM_JOB_NUM_NODES
#SBATCH --get-user-env        # retrieve the login environment variables
###############################################################


###############################################################
# global configurations
###############################################################
EXEC=bin/magi
###############################################################
# problem ID
if [ -z "$PROBLEM" ]; then
    PROBLEM=1
fi
###############################################################
# number of N-body particles
if [ -z "$NTOT" ]; then
    # NTOT=2097152
    NTOT=8388608
fi
###############################################################
# dump file generation interval in the N-body simulation in units of minute (required for GOTHIC)
if [ -z "$SAVE" ]; then
    SAVE=140.0
fi
###############################################################


###############################################################
# problem specific configurations
###############################################################
# FILE: the name of the simulation
# CONFIG: cfg/$CONFIG specifies the physical properties of the initial condition
# EPS: Plummer softening length in units of astrophysical unit system
# ETA: safety parameter to control time stepping (required for GOTHIC)
# FINISH: final time of the N-body simulation in units of astrophysical unit system
# INTERVAL: snapshot interval in the N-body simulation in units of astrophysical unit system (required for GOTHIC)
###############################################################
# dynamical stability of multi components galaxy model (only spherical components)
if [ $PROBLEM -eq 0 ]; then
    FILE=etg
    CONFIG=galaxy/spherical.cfg
    EPS=1.5625e-2
    ETA=0.5
    FINISH=1175.0
    INTERVAL=25.0
fi
###############################################################
# dynamical stability of multi components galaxy model (NFW halo, King bulge, thick Sersic disk, and thin exponential disk)
if [ $PROBLEM -eq 1 ]; then
    FILE=ltg
    CONFIG=galaxy/ltg.cfg
    EPS=1.5625e-2
    ETA=0.5
    FINISH=1175.0
    INTERVAL=25.0
fi
###############################################################
# dynamical stability of a Plummer profile in table form
if [ $PROBLEM -eq 2 ]; then
    FILE=tplummer
    CONFIG=table/tplummer.cfg
    EPS=1.5625e-2
    ETA=0.5
    FINISH=1575.0
    INTERVAL=25.0
fi
###############################################################
# dynamical stability of a Hernquist profile in table form
if [ $PROBLEM -eq 3 ]; then
    FILE=thernquist
    CONFIG=table/thernquist.cfg
    EPS=1.5625e-2
    ETA=0.5
    FINISH=1575.0
    INTERVAL=25.0
fi
###############################################################
# dynamical stability of a Plummer profile in analytic form
if [ $PROBLEM -eq 4 ]; then
    FILE=aplummer
    CONFIG=table/aplummer.cfg
    EPS=1.5625e-2
    ETA=0.5
    FINISH=1575.0
    INTERVAL=25.0
fi
###############################################################
# dynamical stability of a Hernquist profile in analytic form
if [ $PROBLEM -eq 5 ]; then
    FILE=ahernquist
    CONFIG=table/ahernquist.cfg
    EPS=1.5625e-2
    ETA=0.5
    FINISH=1575.0
    INTERVAL=25.0
fi
###############################################################
# set input arguments
OPTION="-file=$FILE -config=$CONFIG -Ntot=$NTOT -eps=$EPS -ft=$FINISH -eta=$ETA -snapshotInterval=$INTERVAL -saveInterval=$SAVE"
###############################################################


###############################################################
# job execution via SLURM
###############################################################
# set stdout and stderr
STDOUT=log/$SLURM_JOB_NAME.$SLURM_JOB_ID.out
STDERR=log/$SLURM_JOB_NAME.$SLURM_JOB_ID.err
###############################################################
# start logging
cd $SLURM_SUBMIT_DIR
echo "use $SLURM_JOB_CPUS_PER_NODE CPUs"
TIME=`date`
echo "start: $TIME"
###############################################################
# execute the job
if [ `which numactl` ]; then
    # run with numactl
    echo "numactl --localalloc $EXEC $OPTION 1>>$STDOUT 2>>$STDERR"
    numactl --localalloc $EXEC $OPTION 1>>$STDOUT 2>>$STDERR
else
    # run without numactl
    echo "$EXEC $OPTION 1>>$STDOUT 2>>$STDERR"
    $EXEC $OPTION 1>>$STDOUT 2>>$STDERR
fi
###############################################################
# finish logging
TIME=`date`
echo "finish: $TIME"
###############################################################
