# $ python py/h5dot.py filename [pmax]
import sys
import numpy as np
import h5py
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt


def locate_panels(ax, nx, ny, share_xaxis, share_yaxis):
    margin = 0.12
    if (share_xaxis == False) or (share_yaxis == False):
        margin = 0.15

    xmin, xmax = margin, 1.0 - margin
    ymin, ymax = margin, 1.0 - margin
    xbin = (xmax - xmin) / nx
    ybin = (ymax - ymin) / ny
    xmargin, ymargin = 0, 0

    if share_yaxis == False:
        xmin = 0.0
        xbin = 1.0 / nx
        xmargin = xbin * margin

    if share_xaxis == False:
        ymin = 0.0
        ybin = 1.0 / ny
        ymargin = ybin * margin

    for ii in range(nx):
        xl = xmin + ii * xbin + xmargin

        for jj in range(ny):
            yl = ymin + jj * ybin + ymargin
            kk = ii * ny + jj
            ax[kk] = fig.add_axes((xl, yl, xbin - 2 * xmargin, ybin - 2 * ymargin))

            if share_xaxis == True:
                ax[kk].tick_params(labelbottom = "off")
                if jj == 0:
                    ax[kk].tick_params(labelbottom = "on")

            if share_yaxis == True:
                ax[kk].tick_params(labelleft = "off")
                if ii == 0:
                    ax[kk].tick_params(labelleft = "on")


# obtain input argument(s)
argv = sys.argv
argc = len(argv)
filename = argv[1]
set_range = True
if argc == 3:
    set_range = False
    pmax = float(argv[2])

# embed fonts
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

# set font size
plt.rcParams['font.size'] = 14

# set number of panels
nxpanel, nypanel = 2, 2
ax = [0] * nxpanel * nypanel

# set figure size and its aspect ratio
Lx = 6
if nxpanel > 2 * nypanel:
    Lx *= 2
Ly = (Lx / nxpanel) * nypanel
fig = plt.figure(figsize = (Lx, Ly))

# set location of panels
locate_panels(ax, nxpanel, nypanel, True, True)

# get number of components and particles
input = "doc/" + filename + ".summary.txt"
fin = open(input, "r")
lines = fin.readlines()
fin.close()
idx = lines[1].find("\t")
kind = int(lines[1][0:idx])# number of components

# get partition
num  = [0] * kind
head = [0] * kind
head[0] = 0
for ii in range(kind):
    idx = lines[2 + ii].find("\n")
    num[ii] = int(lines[2 + ii][0:idx])
    if ii != 0:
        head[ii] = head[ii - 1] + num[ii - 1]

# read particle data
input_file = "dat/" + filename + ".tmp0.h5"
h5file = h5py.File(input_file, "r")
position = h5file["nbody/" + "position"]
steps = h5file["nbody/"].attrs["steps"]
if steps != 0:
    index = h5file["nbody/" + "index"]
h5file.close()

# set plot range
if set_range == True:
    pmax = max(abs(position))

# data preparation
# real *pos;/**< x0, y0, z0, w0, x1, y1, z1, ... */
tx = position[0::4]
ty = position[1::4]
tz = position[2::4]

# sort the particle array if necessary
if steps == 0:
    px = tx
    py = ty
    pz = tz
else:
    px = [0] * len(tx)
    py = [0] * len(tx)
    pz = [0] * len(tx)
    for ii in range(len(tx)):
        px[index[ii]] = tx[ii]
        py[index[ii]] = ty[ii]
        pz[index[ii]] = tz[ii]

# sparse sampling if necessary
skip = 1
nmax = 1048576
if len(tx) > nmax:
    skip = int(np.ceil(len(tx) / nmax))

# set colors of dots
col = [0] * kind
for ii in range(kind):
    if ii % 6 == 0:
        col[ii] = "black"
    if ii % 6 == 1:
        col[ii] = "red"
    if ii % 6 == 2:
        col[ii] = "blue"
    if ii % 6 == 3:
        col[ii] = "magenta"
    if ii % 6 == 4:
        col[ii] = "green"
    if ii % 6 == 5:
        col[ii] = "brown"

# plot particle distribution
for ii in range(nxpanel):
    for jj in range(nypanel):
        idx = ii * nypanel + jj

        if (idx != (nxpanel * nypanel - 1)):
            # ii = 0, jj = 0: xy-plot
            # ii = 0, jj = 1: xz-plot
            # ii = 1, jj = 0: zy-plot
            # ii = 1, jj = 1: blank
            if ii == 0:
                xx = px
            if ii == 1:
                xx = pz
                ax[idx].tick_params(labelleft = False)
            if jj == 0:
                yy = py
            if jj == 1:
                yy = pz
                ax[idx].tick_params(labelbottom = False)

            # plot the data
            for kk in range(kind):
                ax[idx].plot(xx[head[kk]:(head[kk]+num[kk]):skip], yy[head[kk]:(head[kk]+num[kk]):skip], ",", color = col[kk])

            # set plot range
            ax[idx].set_xlim([-pmax, pmax])
            ax[idx].set_ylim([-pmax, pmax])

            # ax[idx].grid()
            ax[idx].tick_params(axis = "both", direction = "in", color = "black", bottom = "on", top = "on", left = "on", right = "on")

            # set label
            if (ii == 0) and (jj == 0):
                ax[idx].set_xlabel(r"$x$")
                ax[idx].set_ylabel(r"$y$")
            if (ii == 0) and (jj == 1):
                ax[idx].set_ylabel(r"$z$")
            if (ii == 1) and (jj == 0):
                ax[idx].set_xlabel(r"$z$")

        else:
            # remove box at the upper right corner
            ax[idx].spines["right"].set_color("none")
            ax[idx].spines["top"].set_color("none")
            ax[idx].tick_params(labelbottom = False, labelleft = False, bottom = False, left = False, right = False, top = False)


# output the figure
plt.savefig("dot.png", format = "png", dpi = 300, bbox_inches = "tight")
