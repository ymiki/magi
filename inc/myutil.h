/**
 * @file myutil.h
 *
 * @brief Header file for utility functions to get command line arguments
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef MYUTIL_H
#define MYUTIL_H


#include <stdbool.h>

#include "macro.h"


/**
 * @def requiredCmdArg(func)
 *
 * @brief wrapper to detect errors
 */
#define requiredCmdArg(err) __requiredCmdArg(err, __FILE__, __LINE__)


static const int myUtilAvail = 1;


/* list of functions appeared in ``myutil.c'' */
int    optionalCmdArg(int err);
void __requiredCmdArg(int err, const char *file, const int line);

int getCmdArgStr(const int argc, const char * const *argv, const char *ref,   char **ret);
int getCmdArgBol(const int argc, const char * const *argv, const char *ref,   bool *ret);
int getCmdArgInt(const int argc, const char * const *argv, const char *ref,    int  *ret);
int getCmdArgUin(const int argc, const char * const *argv, const char *ref,   uint  *ret);
int getCmdArgLng(const int argc, const char * const *argv, const char *ref,   lint  *ret);
int getCmdArgUlg(const int argc, const char * const *argv, const char *ref,  ulong  *ret);
int getCmdArgFlt(const int argc, const char * const *argv, const char *ref,  float  *ret);
int getCmdArgDbl(const int argc, const char * const *argv, const char *ref, double  *ret);

#ifdef DOUBLE_PRECISION
#     define getCmdArgReal(argc, argv, ref, ret) getCmdArgDbl(argc, argv, ref, ret)
#else//DOUBLE_PRECISION
#     define getCmdArgReal(argc, argv, ref, ret) getCmdArgFlt(argc, argv, ref, ret)
#endif//DOUBLE_PRECISION


#endif//MYUTIL_H
