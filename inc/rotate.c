/**
 * @file rotate.c
 *
 * @brief Source code for 3D rotation matrix
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2018/02/15 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "macro.h"
#include "rotate.h"


/**
 * @fn setRodriguesRotationMatrix
 *
 * @brief Set Rodrigues' rotation matrix and its inverse.
 *
 * @param (axis) rotation axis, must be the unit vector
 * @param (sintheta) sine of the rotation angle
 * @param (costheta) cosine of the rotation angle
 * @return (rot) Rodrigues' rotation matrix
 * @return (inv) inverse of Rodrigues' rotation matrix
 */
void setRodriguesRotationMatrix
(real axis[], real sintheta, real costheta, real rot[][3], real inv[][3])
{
  __NOTE__("setRodriguesRotationMatrix start");

  real tmp = UNITY - costheta;
  /** set Rodrigues' matrix */
  rot[0][0] = axis[0] * axis[0] * tmp +           costheta;  rot[0][1] = axis[0] * axis[1] * tmp - axis[2] * sintheta;  rot[0][2] = axis[0] * axis[2] * tmp + axis[1] * sintheta;
  rot[1][0] = axis[1] * axis[0] * tmp + axis[2] * sintheta;  rot[1][1] = axis[1] * axis[1] * tmp +           costheta;  rot[1][2] = axis[1] * axis[2] * tmp - axis[0] * sintheta;
  rot[2][0] = axis[2] * axis[0] * tmp - axis[1] * sintheta;  rot[2][1] = axis[2] * axis[1] * tmp + axis[0] * sintheta;  rot[2][2] = axis[2] * axis[2] * tmp +           costheta;

  /** set inverse matrix of the Rodrigues' matrix */
  inv[0][0] = axis[0] * axis[0] * tmp +           costheta;  inv[0][1] = axis[0] * axis[1] * tmp + axis[2] * sintheta;  inv[0][2] = axis[0] * axis[2] * tmp - axis[1] * sintheta;
  inv[1][0] = axis[1] * axis[0] * tmp - axis[2] * sintheta;  inv[1][1] = axis[1] * axis[1] * tmp +           costheta;  inv[1][2] = axis[1] * axis[2] * tmp + axis[0] * sintheta;
  inv[2][0] = axis[2] * axis[0] * tmp + axis[1] * sintheta;  inv[2][1] = axis[2] * axis[1] * tmp - axis[0] * sintheta;  inv[2][2] = axis[2] * axis[2] * tmp +           costheta;

  __NOTE__("setRodriguesRotationMatrix finish");
}


/**
 * @fn rotateVector
 *
 * @brief Rotate the vector.
 *
 * @param (ini) initial vector
 * @param (mat) rotation matrix
 * @return (ans) rotated vector
 */
void rotateVector(real ini[], real mat[][3], real ans[])
{
  ans[0] = mat[0][0] * ini[0] + mat[0][1] * ini[1] + mat[0][2] * ini[2];
  ans[1] = mat[1][0] * ini[0] + mat[1][1] * ini[1] + mat[1][2] * ini[2];
  ans[2] = mat[2][0] * ini[0] + mat[2][1] * ini[1] + mat[2][2] * ini[2];
}


/**
 * @fn normalizeVector
 *
 * @brief Normalize the vector.
 *
 * @param (vec) initial vector
 * @return (vec) normalized vector
 */
static inline void normalizeVector(real vec[])
{
  real normInv = RSQRT(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
  vec[0] *= normInv;
  vec[1] *= normInv;
  vec[2] *= normInv;
}
/**
 * @fn scalarProd
 *
 * @brief Calculate scalar product.
 *
 * @param (aa) vector A
 * @param (bb) vector B
 * @return scalar product of A and B
 */
static inline real scalarProd(real aa[], real bb[])
{
  return (aa[0] * bb[0] + aa[1] * bb[1] + aa[2] * bb[2]);
}
/**
 * @fn vectorProd
 *
 * @brief Calculate vector product.
 *
 * @param (aa) vector A
 * @param (bb) vector B
 * @return (ans) vector product of A and B
 */
static inline void vectorProd(real aa[], real bb[], real ans[])
{
  ans[0] = aa[1] * bb[2] - aa[2] * bb[1];
  ans[1] = aa[2] * bb[0] - aa[0] * bb[2];
  ans[2] = aa[0] * bb[1] - aa[1] * bb[0];
}


/**
 * @fn retRotationAxis
 *
 * @brief Return rotation axis.
 *
 * @param (bfr) initial vector
 * @param (aft) rotated vector
 * @return (axis) rotation axis
 *
 * @sa normalizeVector
 * @sa vectorProd
 * @sa scalarProd
 */
void retRotationAxis(real bfr[], real aft[], real axis[])
{
  __NOTE__("retRotationAxis start");

  normalizeVector(bfr);  normalizeVector(aft);
  vectorProd(bfr, aft, axis);
  real sintheta = SQRT(scalarProd(axis, axis));/**< norm of axis */
  if( FABS(sintheta) < EPSILON ){
    /** if axis == null vector, then forthcoming process fails */
    axis[0] = UNITY;
    axis[1] = ZERO;
    axis[2] = ZERO;
  }/* if( FABS(sintheta) < EPSILON ){ */
  normalizeVector(axis);

  __NOTE__("retRotationAxis finish");
}


/**
 * @fn initRotationMatrices
 *
 * @brief Initialize Rotation matrix.
 *
 * @param (bfr) initial vector
 * @param (aft) rotated vector
 * @return (rot) rotation matrix
 * @return (inv) inverse of rotation matrix
 *
 * @sa normalizeVector
 * @sa vectorProd
 * @sa scalarProd
 * @sa setRodriguesRotationMatrix
 */
void initRotationMatrices(real bfr[], real aft[], real rot[][3], real inv[][3])
{
  __NOTE__("initRotationMatrices start");

  normalizeVector(bfr);  normalizeVector(aft);
  real axis[3];
  vectorProd(bfr, aft, axis);
  real sintheta = SQRT(scalarProd(axis, axis));/**< norm of axis */
  real costheta = scalarProd(bfr, aft);
  if( FABS(sintheta) < EPSILON ){
    /** if axis == null vector, then forthcoming process fails */
    axis[0] = UNITY;
    axis[1] = ZERO;
    axis[2] = ZERO;
  }/* if( FABS(sintheta) < EPSILON ){ */
  normalizeVector(axis);

  setRodriguesRotationMatrix(axis, sintheta, costheta, rot, inv);

  __NOTE__("initRotationMatrices finish");
}
