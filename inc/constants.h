/**
 * @file constants.h
 *
 * @brief Header file for physical constants and unit system
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef CONSTANTS_H
#define CONSTANTS_H


/** Predefined unit systems */
/** Options are below: */
/**    0: Galactic Nuclei Scale (Msun,  pc, Myr, K) */
/**    1: Galactic Scale        (Msun, kpc, Myr, K) */
/**    2: Stellar Scale         (Msun,  AU,  yr, K) */
/**    3: G = 1 unit            (Msun, kpc, Myr, K) */
/**    4: Host Galaxy Scale     (Msun, kpc, Gyr, K) */
/**    5: GalactICS unit system (Msun, kpc, Myr, K; G = 1) */
/** other: Unity values */
#define UnityValueSystem (-1)
#define GalacticNucleiScale (0)
#define GalacticScale (1)
#define StellarScale (2)
#define UnityNewton (3)
#define HostGalaxyScale (4)
#define GalactICSunit (5)


#define CONSTANTS_H_CHAR_WORDS (16)
#define CONSTANTS_H_PLOT_WORDS (32)


/* list of functions appeared in ``constants.c'' */
void setPhysicalConstantsAndUnitSystem(const int UnitSystem, const int print);


#endif//CONSTANTS_H
