/**
 * @file timer.h
 *
 * @brief Header file for measuring execution time
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef TIMER_H
#define TIMER_H


#include <time.h>


#ifdef  __CUDACC__
extern "C"
{
#endif//__CUDACC__
  double getPresentTimeInSec(void);
  double getPresentTimeInMin(void);
  double getPresentTimeInHour(void);
  double getPresentTimeInDay(void);

  double calcElapsedTimeInSec(struct timespec ini, struct timespec fin);

  void getPresentDateInStrings(char *date);

  void initBenchmark_cpu(void          );
  void stopBenchmark_cpu(double *result);
#ifdef  __CUDACC__
}
#endif//__CUDACC__


#endif//TIMER_H
