/**
 * @file timer.c
 *
 * @brief Source code for measuring execution time
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "timer.h"


static const double sec2min  = 1.66666666667e-2;
static const double sec2hour = 2.77777777778e-4;
static const double sec2day  = 1.15740740740e-5;


/**
 * @fn getPresentTimeInSec
 *
 * @brief Get present time in units of second.
 *
 * @return present time
 */
double getPresentTimeInSec(void)
{
  struct timespec tv;
  clock_gettime(CLOCK_MONOTONIC_RAW, &tv);
  return (tv.tv_sec + 1.0e-9 * (double)tv.tv_nsec);
}
/**
 * @fn getPresentTimeInMin
 *
 * @brief Get present time in units of minute.
 *
 * @return present time
 *
 * @sa getPresentTimeInSec
 */
double getPresentTimeInMin(void)
{
  return (sec2min * getPresentTimeInSec());
}
/**
 * @fn getPresentTimeInHour
 *
 * @brief Get present time in units of hour.
 *
 * @return present time
 *
 * @sa getPresentTimeInSec
 */
double getPresentTimeInHour(void)
{
  return (sec2hour * getPresentTimeInSec());
}
/**
 * @fn getPresentTimeInDay
 *
 * @brief Get present time in units of day.
 *
 * @return present time
 *
 * @sa getPresentTimeInSec
 */
double getPresentTimeInDay(void)
{
  return (sec2day * getPresentTimeInSec());
}


/**
 * @fn calcElapsedTimeInSec
 *
 * @brief Calculation elapsed time in units of second.
 *
 * @param (ini) initial time
 * @param (fin) finish time
 * @return elapsed time from initial time to finish time
 */
double calcElapsedTimeInSec(struct timespec ini, struct timespec fin)
{
  return ((fin.tv_sec - ini.tv_sec) + 1.0e-9 * (double)(fin.tv_nsec - ini.tv_nsec));
}


/**
 * @fn getPresentDateInStrings
 *
 * @brief Get present date in strings.
 *
 * @return present date
 */
void getPresentDateInStrings(char *date)
{
  time_t timer;
  time(&timer);
  sprintf(date, "%s", ctime(&timer));
}


static struct timespec stampIni, stampFin;
/**
 * @fn initBenchmark_cpu
 *
 * @brief Start benchmark.
 */
void initBenchmark_cpu(void          ){  clock_gettime(CLOCK_MONOTONIC_RAW, &stampIni);}
/**
 * @fn stopBenchmark_cpu
 *
 * @brief Stop benchmark.
 *
 * @return (result) execution time of the benchmark
 *
 * @sa calcElapsedTimeInSec
 */
void stopBenchmark_cpu(double *result){  clock_gettime(CLOCK_MONOTONIC_RAW, &stampFin);  *result += calcElapsedTimeInSec(stampIni, stampFin);}
