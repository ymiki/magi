/**
 * @file mpilib.c
 *
 * @brief Source code for utility packages to use MPI
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2018/05/14 (Mon)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "macro.h"
#include "mpilib.h"


/**
 * @fn __chkMPIerr
 *
 * @brief wrapper for calling MPI functions
 *
 * @param (err) function call
 * @param (file) name of the file contains the called function
 * @param (line) line of the function in the specified file
 */
void __chkMPIerr(int err, const char *file, const int line)
{
  if( err != MPI_SUCCESS ){
    fprintf(stderr, "Error is detected at %s(%d)\n", file, line);
    __KILL__(stderr, "MPI error, error ID is %d(0x%x)\n", err, err);
  }
}


/**
 * @fn initMPI
 *
 * @brief Initialize MPI.
 *
 * @return (mpi) information on MPI process
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 *
 * @sa setMPI
 */
void initMPI(MPIinfo *mpi, int *argc, char ***argv)
{
  __NOTE__("initMPI start");

#ifdef  _OPENMP
  static int required, provided;
  required = MPI_THREAD_MULTIPLE;
  chkMPIerr(MPI_Init_thread(argc, argv, required, &provided));
  if( provided != required ){
    __KILL__(stderr, "provided option is %d while required option is %d.\nread additional information on man page of MPI_Init_thread.\n\t%d: MPI_THREAD_SINGLE,\n\t%d: MPI_THREAD_FUNNELED,\n\t%d: MPI_THREAD_SERIALIZED,\n\t%d: MPI_THREAD_MULTIPLE.\n",
	     provided, required,
	     MPI_THREAD_SINGLE, MPI_THREAD_FUNNELED, MPI_THREAD_SERIALIZED, MPI_THREAD_MULTIPLE);
  }
#else///_OPENMP
  chkMPIerr(MPI_Init(argc, argv));
#endif//_OPENMP
  mpi->comm = MPI_COMM_WORLD;
  mpi->info = MPI_INFO_NULL;
  setMPI(mpi);

  __NOTE__("initMPI finish");
}
/**
 * @fn exitMPI
 *
 * @brief Finalize MPI.
 *
 */
void exitMPI(void)
{
  __NOTE__("exitMPI start");
  chkMPIerr(MPI_Finalize());
  __NOTE__("exitMPI start");
}


/**
 * @fn setMPI
 *
 * @brief Set rank of the MPI process and number of MPI processes.
 *
 * @return (mpi) information on MPI process
 */
void setMPI(MPIinfo *mpi)
{
  __NOTE__("setMPI start");

  chkMPIerr(MPI_Comm_size(mpi->comm, &(mpi->size)));
  chkMPIerr(MPI_Comm_rank(mpi->comm, &(mpi->rank)));

  __NOTE__("setMPI finish");
}


/**
 * @fn splitMPI
 *
 * @brief Split MPI communicator.
 *
 * @param (old) current MPI communicator
 * @param (newGroupIdx) index of the new MPI communicator
 * @param (newRankOrder) index to determine the new MPI rank in the new communicator
 * @return (newgroup) new MPI communicator
 *
 * @sa setMPI
 */
void splitMPI(MPI_Comm old, int newGroupIdx, int newRankOrder, MPIinfo *newgroup)
{
  __NOTE__("%s\n", "splitMPI start");

  chkMPIerr(MPI_Comm_split(old, newGroupIdx, newRankOrder, &(newgroup->comm)));
  setMPI(newgroup);
  newgroup->info = MPI_INFO_NULL;

  __NOTE__("%s\n", "splitMPI finish");
}
/**
 * @fn freeMPIgroup
 *
 * @brief Free MPI communicator.
 *
 * @param MPI communicator
 */
void freeMPIgroup(MPIinfo *old)
{
  __NOTE__("%s\n", "splitMPI start");
  chkMPIerr(MPI_Comm_free(&(old->comm)));
  __NOTE__("%s\n", "splitMPI finish");
}


/**
 * @fn commitMPItype
 *
 * @brief Commit new data type for MPI.
 *
 * @return (newtype) new data type (new = old * num)
 * @param (oldtype) old data type
 * @param (count) number of elements
 */
void commitMPItype(MPI_Datatype *newtype, MPI_Datatype oldtype, int count)
{
  __NOTE__("%s\n", "start");

  chkMPIerr(MPI_Type_contiguous(count, oldtype, newtype));
  chkMPIerr(MPI_Type_commit(newtype));

  __NOTE__("%s\n", "end");
}

/**
 * @fn commitMPIstruct
 *
 * @brief Commit structure for MPI.
 *
 * @param (num) number of elements
 * @param (blck) number of blocks for each element
 * @param (disp) displacements of each element
 * @param (type) data type of each element
 * @return (newtype) new data type
 */
void commitMPIstruct(int num, int *blck, MPI_Aint *disp, MPI_Datatype *type, MPI_Datatype *newtype)
{
  __NOTE__("%s\n", "commitMPIstruct start");

  chkMPIerr(MPI_Type_create_struct(num, blck, disp, type, newtype));
  chkMPIerr(MPI_Type_commit(newtype));

  __NOTE__("%s\n", "commitMPIstruct finish");
}

/**
 * @fn commitMPIbyte
 *
 * @brief Commit new data type for MPI.
 *
 * @return (newtype) new data type
 * @param (size) size of new data
 */
void commitMPIbyte(MPI_Datatype *newtype, int size)
{
  static MPI_Aint     disp = 0;
  static MPI_Datatype type = MPI_BYTE;
  commitMPIstruct(1, &size, &disp, &type, newtype);
}

/**
 * @fn freeMPIstruct
 *
 * @brief Deallocate MPI data type.
 *
 * @param (oldtype) MPI data type
 */
void freeMPIstruct(MPI_Datatype *oldtype)
{
  __NOTE__("%s\n", "freeMPIstruct start");
  chkMPIerr(MPI_Type_free(oldtype));
  __NOTE__("%s\n", "freeMPIstruct finish");
}
