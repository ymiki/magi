/**
 * @file rand.h
 *
 * @brief Header file for utility package to switch pseudo random number generators
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef RAND_H
#define RAND_H


#include "macro.h"


/**
 * @typedef rand_state
 *
 * @brief state of the pseudo random numbers (sfmt_t or gsl_rng)
 */
/**
 * @def GENRAND64(a)
 *
 * @brief generate pseudo random number in 64 bits
 */
/**
 * @def UNIRAND_DBL(a)
 *
 * @brief generate pseudo random number in [0, 1) with double-precision floating-point number
 */
#ifdef  USE_SFMT
#include "SFMT.h"
typedef sfmt_t rand_state;
/* #define GENRAND32(a) (sfmt_genrand_uint32(a)) */
#define GENRAND64(a) (sfmt_genrand_uint64(a))
#define UNIRAND_DBL(a) (sfmt_genrand_res53(a))
#else///USE_SFMT
#include <gsl/gsl_rng.h>
typedef gsl_rng rand_state;
#define GENRAND64(a) (gsl_rng_get(a))
#define UNIRAND_DBL(a) (gsl_rng_uniform(a))
#endif//USE_SFMT


/**
 * @def UNIRAND(a)
 *
 * @brief generate pseudo random number in [0, 1) with floating-point number (real)
 */
#define UNIRAND(a) (CAST_D2R(UNIRAND_DBL(a)))
/**
 * @def RANDVAL(a)
 *
 * @brief generate pseudo random number in [-1, 1) with floating-point number (real)
 */
#define RANDVAL(a) (TWO * (UNIRAND(a)) - UNITY)
/**
 * @def RANDVAL_DBL(a)
 *
 * @brief generate pseudo random number in [-1, 1) with double-precision floating-point number
 */
#define RANDVAL_DBL(a) (2.0 * (UNIRAND_DBL(a)) - 1.0)


/* list of functions appeared in ``rand.c'' */
#ifdef  __CUDACC__
extern "C"
{
#endif//__CUDACC__
  void initRandNum(rand_state **rand);
  void freeRandNum(rand_state  *rand);
#ifdef  __CUDACC__
}
#endif//__CUDACC__


#endif//RAND_H
