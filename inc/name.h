/**
 * @file name.h
 *
 * @brief Header file for names of folders and files
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2018/01/16 (Tue)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef NAME_H
#define NAME_H


/** macros to specify the file name */
#define DATAFOLDER "dat"
#define DOCUMENTFOLDER "doc"
#define FIGUREFOLDER "fig"
#define LOGFOLDER "log"
#define CFGFOLDER "cfg"
#define BENCH_LOG_FOLDER "bench"


#endif//NAME_H
