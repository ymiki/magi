/**
 * @file myutil.c
 *
 * @brief Source code for utility functions to get command line arguments
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "macro.h"
#include "myutil.h"


static const int myUtilSuccess = 0;
static const int myUtilFailure = 1;

static char flag[256];
static char type[256];


#ifdef __ICC
/* Disable ICC's remark #161: unrecognized #pragma */
#     pragma warning (disable:161)
#endif//__ICC


/**
 * @fn optionalCmdArg
 *
 * @brief Wrapper function for optional arguments.
 *
 * @param (err) function call
 * @return the specified argument is available or not
 */
int optionalCmdArg(int err)
{
  if( err == myUtilSuccess )    return   (myUtilAvail);
  else                          return (!(myUtilAvail));
}
/**
 * @fn __requiredCmdArg
 *
 * @brief Wrapper function for required arguments.
 *
 * @param (err) function call
 * @param (file) name of the file contains the called function
 * @param (line) line of the function in the specified file
 */
void __requiredCmdArg(int err, const char *file, const int line)
{
  if( err != myUtilSuccess ){
    fprintf(stderr, "%s(%d): required command line argument \"-%s=<%s>\" is missing.\n", file, line, flag, type);
    __KILL__(stderr, "%s\n", "error in $INCDIR/util.c");
  }
}


/**
 * @fn findCmdArgFlg
 *
 * @brief Search the specified flag.
 *
 * @param (flg) the target argument
 * @param (str) list of the input arguments
 * @return (pos) position of the target argument if available
 * @return the target argument is available or not
 */
static inline int findCmdArgFlg(char flg, const char *str, int *pos)
{
  for(*pos = 0; *pos < (int)strlen(str); *pos += 1){
    if( str[*pos] == flg ){
      *pos += 1;
      return (myUtilSuccess);
    }
  }

  return (myUtilFailure);
}


/**
 * @fn getCmdArgStr
 *
 * @brief Get command line argument as a string.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgStr(const int argc, const char * const *argv, const char *ref,   char **ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "char");
  *ret = NULL;

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = &tmp[len + 1];/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}
/**
 * @fn getCmdArgBol
 *
 * @brief Get command line argument as a bool.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgBol(const int argc, const char * const *argv, const char *ref,   bool *ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "bool");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = (bool)atoi(&tmp[len + 1]);/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}
/**
 * @fn getCmdArgInt
 *
 * @brief Get command line argument as an integer.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgInt(const int argc, const char * const *argv, const char *ref,    int *ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "int");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = atoi(&tmp[len + 1]);/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}
/**
 * @fn getCmdArgUin
 *
 * @brief Get command line argument as an unsigned integer.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgUin(const int argc, const char * const *argv, const char *ref,   uint *ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "unsigned int");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = (uint)atoi(&tmp[len + 1]);/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}
/**
 * @fn getCmdArgLng
 *
 * @brief Get command line argument as a long integer.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgLng(const int argc, const char * const *argv, const char *ref,   lint *ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "long int");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = atol(&tmp[len + 1]);/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}
/**
 * @fn getCmdArgUlg
 *
 * @brief Get command line argument as an unsigned long integer.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgUlg(const int argc, const char * const *argv, const char *ref,  ulong *ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "unsigned long int");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = (ulong)atol(&tmp[len + 1]);/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}
/**
 * @fn getCmdArgFlt
 *
 * @brief Get command line argument as a float.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgFlt(const int argc, const char * const *argv, const char *ref,  float *ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "float");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = (float)atof(&tmp[len + 1]);/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}
/**
 * @fn getCmdArgDbl
 *
 * @brief Get command line argument as a double.
 *
 * @param (argc) number of input arguments
 * @param (argv) input arguments
 * @param (ref) the target argument
 * @return the obtained argument
 * @return the specified argument is available or not
 */
int getCmdArgDbl(const int argc, const char * const *argv, const char *ref, double *ret)
{
  sprintf(flag, "%s", ref);  sprintf(type, "%s", "double");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  if( argc > 1 )
    for(int ii = 1; ii < argc; ii++){
      int idx;
      if( myUtilSuccess == findCmdArgFlg('-', argv[ii], &idx) ){
	char *tmp = (char *)&argv[ii][idx];
	int len = (int)strlen(ref);
	if( (strncmp(ref, tmp, len) == 0) && ((tmp[len] == '=') || ((int)strlen(tmp) == len)) ){
	  *ret = atof(&tmp[len + 1]);/* 1 is (int)strlen("=") */
	  return (myUtilSuccess);
	}
      }
    }
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)

  return (myUtilFailure);
}


#ifdef  __ICC
/* Enable ICC's remark #161: unrecognized #pragma */
#     pragma warning (enable:161)
#endif//__ICC
