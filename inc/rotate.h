/**
 * @file rotate.h
 *
 * @brief Header file for 3D rotation matrix
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef ROTATE_H
#define ROTATE_H


#include "macro.h"


/* list of functions appeared in ``rotate.c'' */
void setRodriguesRotationMatrix
(real axis[], real sintheta, real costheta, real rot[][3], real inv[][3]);
void retRotationAxis(real bfr[], real aft[], real axis[]);

void rotateVector(real ini[], real mat[][3], real ans[]);
void initRotationMatrices(real bfr[], real aft[], real rot[][3], real inv[][3]);


#endif//ROTATE_H
