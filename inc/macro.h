/**
 * @file macro.h
 *
 * @brief Header file for macros
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2018/08/23 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef MACRO_H
#define MACRO_H


/**
 * @def SIMD_BITS
 *
 * @brief SIMD width in units of bits
 */
#ifdef  __AVX__
#define SIMD_BITS (256)
#else///__AVX__
#ifdef  __SSE__
#define SIMD_BITS (128)
#else///__SSE__
#ifdef  __ALTIVEC__
#define SIMD_BITS (128)
#endif//__ALTIVEC__
#endif//__SSE__
#endif//__AVX__

/**
 * @def NSIMD
 *
 * @brief SIMD width in terms of the number of floating-point numbers
 */
#ifdef  DOUBLE_PRECISION
/* 2^6 = 64 bits */
#define NSIMD (SIMD_BITS >> 6)
#else///DOUBLE_PRECISION
/* 2^5 = 32 bits */
#define NSIMD (SIMD_BITS >> 5)
#endif//DOUBLE_PRECISION

/**
 * @def ALIGN_BYTES
 * @def ALIGNED
 * @def VECARRAY
 *
 * @brief utilities for memory alignment
 */
/* 2^3 = 8 bits = 1 byte */
#define ALIGN_BYTES (SIMD_BITS >> 3)
#   if  ALIGN_BYTES > __BIGGEST_ALIGNMENT__
#undef  ALIGN_BYTES
#define ALIGN_BYTES  (__BIGGEST_ALIGNMENT__)
#endif//ALIGN_BYTES > __BIGGEST_ALIGNMENT__
#define ALIGNED __align__(ALIGN_BYTES)
#ifdef  __ICC
#define VECARRAY(a) __assume_aligned((a), ALIGN_BYTES)
#else///__ICC
#define VECARRAY(a)
#endif//__ICC


#include <float.h>
#include <limits.h>


/* useful constants taken from gsl/gsl_math.h */
#ifndef M_E
#define M_E        2.71828182845904523536028747135      /**< e */
#endif//M_E
#ifndef M_LOG2E
#define M_LOG2E    1.44269504088896340735992468100      /**< log_2 (e) */
#endif//M_LOG2E
#ifndef M_LOG10E
#define M_LOG10E   0.43429448190325182765112891892      /**< log_10 (e) */
#endif//M_LOG10E
#ifndef M_SQRT2
#define M_SQRT2    1.41421356237309504880168872421      /**< sqrt(2) */
#endif//M_SQRT2
#ifndef M_SQRT1_2
#define M_SQRT1_2  0.70710678118654752440084436210      /**< sqrt(1/2) */
#endif//M_SQRT1_2
#ifndef M_SQRT3
#define M_SQRT3    1.73205080756887729352744634151      /**< sqrt(3) */
#endif//M_SQRT3
#ifndef M_PI
#define M_PI       3.14159265358979323846264338328      /**< pi */
#endif//M_PI
#ifndef M_PI_2
#define M_PI_2     1.57079632679489661923132169164      /**< pi/2 */
#endif//M_PI_2
#ifndef M_PI_4
#define M_PI_4     0.78539816339744830961566084582      /**< pi/4 */
#endif//M_PI_4
#ifndef M_SQRTPI
#define M_SQRTPI   1.77245385090551602729816748334      /**< sqrt(pi) */
#endif//M_SQRTPI
#ifndef M_2_SQRTPI
#define M_2_SQRTPI 1.12837916709551257389615890312      /**< 2/sqrt(pi) */
#endif//M_2_SQRTPI
#ifndef M_1_PI
#define M_1_PI     0.31830988618379067153776752675      /**< 1/pi */
#endif//M_1_PI
#ifndef M_2_PI
#define M_2_PI     0.63661977236758134307553505349      /**< 2/pi */
#endif//M_2_PI
#ifndef M_LN10
#define M_LN10     2.30258509299404568401799145468      /**< ln(10) */
#endif//M_LN10
#ifndef M_LN2
#define M_LN2      0.69314718055994530941723212146      /**< ln(2) */
#endif//M_LN2
#ifndef M_LNPI
#define M_LNPI     1.14472988584940017414342735135      /**< ln(pi) */
#endif//M_LNPI
#ifndef M_EULER
#define M_EULER    0.57721566490153286060651209008      /**< Euler constant */
#endif//M_EULER


/* useful constants taken from math.h */
#ifndef M_El
#define M_El        2.7182818284590452353602874713526625L  /**< e */
#endif//M_El
#ifndef M_LOG2El
#define M_LOG2El    1.4426950408889634073599246810018921L  /**< log_2 e */
#endif//M_LOG2El
#ifndef M_LOG10El
#define M_LOG10El   0.4342944819032518276511289189166051L  /**< log_10 e */
#endif//M_LOG10El
#ifndef M_LN2l
#define M_LN2l      0.6931471805599453094172321214581766L  /**< log_e 2 */
#endif//M_LN2l
#ifndef M_LN10l
#define M_LN10l     2.3025850929940456840179914546843642L  /**< log_e 10 */
#endif//M_LN10l
#ifndef M_PIl
#define M_PIl       3.1415926535897932384626433832795029L  /**< pi */
#endif//M_PIl
#ifndef M_PI_2l
#define M_PI_2l     1.5707963267948966192313216916397514L  /**< pi/2 */
#endif//M_PI_2l
#ifndef M_PI_4l
#define M_PI_4l     0.7853981633974483096156608458198757L  /**< pi/4 */
#endif//M_PI_4l
#ifndef M_1_PIl
#define M_1_PIl     0.3183098861837906715377675267450287L  /**< 1/pi */
#endif//M_1_PIl
#ifndef M_2_PIl
#define M_2_PIl     0.6366197723675813430755350534900574L  /**< 2/pi */
#endif//M_2_PIl
#ifndef M_2_SQRTPIl
#define M_2_SQRTPIl 1.1283791670955125738961589031215452L  /**< 2/sqrt(pi) */
#endif//M_2_SQRTPIl
#ifndef M_SQRT2l
#define M_SQRT2l    1.4142135623730950488016887242096981L  /**< sqrt(2) */
#endif//M_SQRT2l
#ifndef M_SQRT1_2l
#define M_SQRT1_2l  0.7071067811865475244008443621048490L  /**< 1/sqrt(2) */
#endif//M_SQRT1_2l



#ifndef __CUDACC__
/**
 * @def __align__(n)
 *
 * @brief same format with CUDA
 */
#define __align__(n) \
        __attribute__((aligned(n)))
#endif//__CUDACC__


/**
 * @def INDEX2D(nx, ny,     ii, jj    )
 * @def INDEX3D(nx, ny, nz, ii, jj, kk)
 * @def INDEX4D(nx, ny, nz, nw, ii, jj, kk, ll)
 * @def INDEX  (nx, ny, nz, ii, jj, kk)
 *
 * @brief point 1D array like as multi-dimensional array
 */
#define INDEX2D(nx, ny,     ii, jj    ) (              ((jj) + (ny) * (ii)))
#define INDEX3D(nx, ny, nz, ii, jj, kk) ((kk) + (nz) * ((jj) + (ny) * (ii)))
#define INDEX4D(nx, ny, nz, nw, ii, jj, kk, ll) (INDEX2D(nx * ny * nz, nw, INDEX3D(nx, ny, nz, ii, jj, kk), ll))
#define INDEX(nx, ny, nz, ii, jj, kk) (INDEX3D(nx, ny, nz, ii, jj, kk))


/**
 * @def CALL_FUNC_WITH_NAME(f)
 *
 * @brief call function with its name
 */
#define CALL_FUNC_WITH_NAME(f) f,#f


#ifndef __CUDACC__
/**
 * @def __FPRINTF__(dst, ...)
 *
 * @brief fprintf function with file name, line, and function
 */
#define __FPRINTF__(dst, ...)				\
  {								\
    fprintf(dst, "%s(%d): %s: ", __FILE__, __LINE__, __func__);	\
    fprintf(dst, ##__VA_ARGS__);				\
    fflush(NULL);						\
  }
/**
 * @def __PRINTF__(...)
 *
 * @brief printf function with file name, line, and function
 */
#define __PRINTF__(...)					\
  {								\
    printf("%s(%d): %s: ", __FILE__, __LINE__, __func__);	\
    printf(##__VA_ARGS__);					\
    fflush(NULL);						\
  }
#else///__CUDACC__
/**
 * @def __FPRINTF__(dst, ...)
 *
 * @brief fprintf function with file name, line, and function
 */
#define __FPRINTF__(dst, ...)				\
  {								\
    fprintf(dst, "%s(%d): %s: ", __FILE__, __LINE__, __func__);	\
    fprintf(dst, __VA_ARGS__);				\
    fflush(NULL);						\
  }
/**
 * @def __PRINTF__(...)
 *
 * @brief printf function with file name, line, and function
 */
#define __PRINTF__(...)					\
  {								\
    printf("%s(%d): %s: ", __FILE__, __LINE__, __func__);	\
    printf(__VA_ARGS__);					\
    fflush(NULL);						\
  }
#endif//__CUDACC__


/**
 * @def __KILL__(dst, ...)
 *
 * @brief abort the simulation with messages
 */
#ifndef __CUDACC__
#   if  defined(MPI_INCLUDED) || defined(OMPI_MPI_H)
#define __KILL__(dst, ...)		\
  {						\
    __FPRINTF__(dst, ##__VA_ARGS__);		\
    MPI_Abort(MPI_COMM_WORLD, 1);		\
  }
#else///defined(MPI_INCLUDED) || defined(OMPI_MPI_H)
#define __KILL__(dst, ...)		\
  {						\
    __FPRINTF__(dst, ##__VA_ARGS__);		\
    exit(EXIT_FAILURE);				\
  }
#endif///defined(MPI_INCLUDED) || defined(OMPI_MPI_H)
#else///__CUDACC__
#   if  defined(MPI_INCLUDED) || defined(OMPI_MPI_H)
#define __KILL__(dst, ...)		\
  {						\
    __FPRINTF__(dst, __VA_ARGS__);		\
    MPI_Abort(MPI_COMM_WORLD, 1);		\
  }
#else///defined(MPI_INCLUDED) || defined(OMPI_MPI_H)
#define __KILL__(dst, ...)		\
  {						\
    __FPRINTF__(dst, __VA_ARGS__);		\
    exit(EXIT_FAILURE);				\
  }
#endif//defined(MPI_INCLUDED) || defined(OMPI_MPI_H)
#endif//__CUDACC__


/**
 * @def __NOTE__(...)
 *
 * @brief print out messages when NDEBUG is not defined
 */
#ifndef NDEBUG
#      ifndef __CUDACC__
#define __NOTE__(...)    __FPRINTF__(stdout, ##__VA_ARGS__)
#      else///__CUDACC__
#define __NOTE__(...)    __FPRINTF__(stdout, __VA_ARGS__)
#      endif//__CUDACC__
#else//NDEBUG
#define __NOTE__(...)
#endif//NDEBUG


/**
 * @typedef sint
 *
 * @brief short int
 */
typedef short int sint;
/**
 * @typedef lint
 *
 * @brief long int
 */
typedef long int lint;
/**
 * @typedef uchar
 *
 * @brief unsigned char
 */
typedef unsigned char uchar;
/**
 * @typedef uint
 *
 * @brief unsigned int
 */
typedef unsigned int uint;
/**
 * @typedef ulong
 *
 * @brief unsigned long int
 */
typedef unsigned long int ulong;
/**
 * @typedef ushort
 *
 * @brief unsigned short int
 */
typedef unsigned short int ushort;


/** Definition about single/double-precision floating-point numbers, and related functions */
#ifdef  DOUBLE_PRECISION
/**
 * @typedef real
 *
 * @brief double
 */
typedef double real;

/** frequently used floating-point numbers */
#      ifndef PAULSLIB_H
/* avoid colliding macro with S2PLOT package */
#define EPSILON    (DBL_EPSILON)
#      endif//PAULSLIB_H
#define REAL_MIN   (-DBL_MAX)
#define REAL_MAX   ( DBL_MAX)
#define ZERO       (0.0)
#define UNITY      (1.0)
#define TWO        (2.0)
#define THREE      (3.0)
#define FOUR       (4.0)
#define FIVE       (5.0)
#define SIX        (6.0)
#define SEVEN      (7.0)
#define EIGHT      (8.0)
#define NINE       (9.0)
#define TEN        (10.0)
#define HALF       (0.5)
#define ONE_THIRD  (3.3333333333333333e-1)
#define QUARTER    (0.25)
#define ONE_FIFTH  (0.2)
#define ONE_SIXTH  (1.6666666666666667e-1)
#define ONE_EIGHTH (0.125)
/* power, exponential, logarithmic functions */
#define EXP(a)    (exp(a))
#define EXP2(a)   (exp2(a))
#define EXP10(a)  (exp10(a))
#define EXPM1(a)  (expm1(a))
#define ERF(a)    (erf(a))
#define ERFC(a)   (erfc(a))
#define POW(a, b) (pow((a), (b)))
#define POW2(a)   (EXP2(a))
#define LOG(a)    (log(a))
#define LOG10(a)  (log10(a))
#      ifndef PAULSLIB_H
/* avoid colliding macro with S2PLOT package */
#define LOG2(a)   (log2(a))
#      endif//PAULSLIB_H
#define LOG1P(a)  (log1p(a))
/* root functions */
#define   SQRT(a)    ( sqrt(a))
#define   CBRT(a)    ( cbrt(a))
#define  HYPOT(a, b) (hypot((a), (b)))
#      ifdef  __CUDACC__
#define  RSQRT(a)    ( rsqrt(a))
#define  RCBRT(a)    ( rcbrt(a))
#define RHYPOT(a, b) (rhypot((a), (b)))
#      endif//__CUDACC__
/* trigonometric functions */
#define  SIN(a) ( sin(a))
#define  COS(a) ( cos(a))
#define  TAN(a) ( tan(a))
#define ASIN(a) (asin(a))
#define ACOS(a) (acos(a))
#define ATAN(a) (atan(a))
#define SINCOS(x, s, c) (sincos((x), (s), (c)))
#      ifdef  __CUDACC__
#define SINPI(a) (sinpi(a))
#define COSPI(a) (cospi(a))
#define SINCOSPI(x, s, c) (sincospi((x), (s), (c)))
#      else///__CUDACC__
#define SINPI(a) (sinpi(M_PI * (a)))
#define COSPI(a) (cospi(M_PI * (a)))
#define SINCOSPI(x, s, c) (sincos(M_PI * (x), (s), (c)))
#      endif//__CUDACC__
/* hyperbolic functions */
#define  SINH(a) ( sinh(a))
#define  COSH(a) ( cosh(a))
#define  TANH(a) ( tanh(a))
#define ASINH(a) (asinh(a))
#define ACOSH(a) (acosh(a))
#define ATANH(a) (atanh(a))
/* useful functions */
#define LDEXP(a, b) (ldexp((a), (b)))
#define  FMIN(a, b) ( fmin((a), (b)))
#define  FMAX(a, b) ( fmax((a), (b)))
#define  MODF(x, i) ( modf((x), (i)))
#define  FDIM(a, b) ( fdim((a), (b)))
/* functions related to sign of a number */
#define     FABS(a)    (    fabs(a))
#define COPYSIGN(a, b) (copysign((a), (b)))
/* round functions */
#define     FLOOR(a) (    floor(a))
#define      CEIL(a) (     ceil(a))
#define     TRUNC(a) (    trunc(a))
#define     ROUND(a) (    round(a))
#define NEARBYINT(a) (nearbyint(a))
/* cast functions */
#define CAST_R2F(a) (float)(a)
#define CAST_F2R(a) (double)(a)
#define CAST_R2D(a) (a)
#define CAST_D2R(a) (a)

#else//DOUBLE_PRECISION
/**
 * @typedef real
 *
 * @brief float
 */
typedef float real;

/** frequently used floating-point numbers */
#      ifndef PAULSLIB_H
/* avoid colliding macro with S2PLOT package */
#define EPSILON    (FLT_EPSILON)
#      endif//PAULSLIB_H
#define REAL_MIN   (-FLT_MAX)
#define REAL_MAX   ( FLT_MAX)
#define ZERO       (0.0f)
#define UNITY      (1.0f)
#define TWO        (2.0f)
#define THREE      (3.0f)
#define FOUR       (4.0f)
#define FIVE       (5.0f)
#define SIX        (6.0f)
#define SEVEN      (7.0f)
#define EIGHT      (8.0f)
#define NINE       (9.0f)
#define TEN        (10.0f)
#define HALF       (0.5f)
#define ONE_THIRD  (3.333333e-1f)
#define QUARTER    (0.25f)
#define ONE_FIFTH  (0.2f)
#define ONE_SIXTH  (1.666667e-1f)
#define ONE_EIGHTH (0.125f)
/* power, exponential, logarithmic functions */
#define EXP(a)    (expf(a))
#define EXP2(a)   (exp2f(a))
#define EXP10(a)  (exp10f(a))
#define EXPM1(a)  (expm1f(a))
#define ERF(a)    (erff(a))
#define ERFC(a)   (erfcf(a))
#define POW(a, b) (powf(a, b))
#define POW2(a)   (EXP2(a))
#define LOG(a)    (logf(a))
#define LOG10(a)  (log10f(a))
#      ifndef PAULSLIB_H
/* avoid colliding macro with S2PLOT package */
#define LOG2(a)   (log2f(a))
#      endif//PAULSLIB_H
#define LOG1P(a)  (log1pf(a))
/* root functions */
#define   SQRT(a)    ( sqrtf(a))
#define   CBRT(a)    ( cbrtf(a))
#define  HYPOT(a, b) (hypotf((a), (b)))
#      ifdef __CUDACC__
#define  RSQRT(a)    ( rsqrtf(a))
#define  RCBRT(a)    ( rcbrtf(a))
#define RHYPOT(a, b) (rhypotf((a), (b)))
#      endif//__CUDACC__
/* trigonometric functions */
#define  SIN(a) ( sinf(a))
#define  COS(a) ( cosf(a))
#define  TAN(a) ( tanf(a))
#define ASIN(a) (asinf(a))
#define ACOS(a) (acosf(a))
#define ATAN(a) (atanf(a))
#define SINCOS(x, s, c) (sincosf((x), (s), (c)))
#      ifdef  __CUDACC__
#define SINPI(a) (sinpif(a))
#define COSPI(a) (cospif(a))
#define SINCOSPI(x, s, c) (sincospif((x), (s), (c)))
#      else///__CUDACC__
#define SINPI(a) (sinpif((float)M_PI * (a)))
#define COSPI(a) (cospif((float)M_PI * (a)))
#define SINCOSPI(x, s, c) (sincosf((float)M_PI * (x), (s), (c)))
#      endif//__CUDACC__
/* hyperbolic functions */
#define  SINH(a) ( sinhf(a))
#define  COSH(a) ( coshf(a))
#define  TANH(a) ( tanhf(a))
#define ASINH(a) (asinhf(a))
#define ACOSH(a) (acoshf(a))
#define ATANH(a) (atanhf(a))
/* useful functions */
#define LDEXP(a, b) (ldexpf((a), (b)))
#define  FMIN(a, b) ( fminf((a), (b)))
#define  FMAX(a, b) ( fmaxf((a), (b)))
#define  MODF(x, i) ( modff((x), (i)))
#define  FDIM(a, b) ( fdimf((a), (b)))
/* functions related to sign of a number */
#define     FABS(a)    (    fabsf(a))
#define COPYSIGN(a, b) (copysignf((a), (b)))
/* round functions */
#define     FLOOR(a) (    floorf(a))
#define      CEIL(a) (     ceilf(a))
#define     TRUNC(a) (    truncf(a))
#define     ROUND(a) (    roundf(a))
#define NEARBYINT(a) (nearbyintf(a))
/* cast functions */
#define CAST_R2F(a) (a)
#define CAST_F2R(a) (a)
#define CAST_R2D(a) (double)(a)
#define CAST_D2R(a) (float)(a)
#endif//DOUBLE_PRECISION


/** Combined functions */
#      ifdef  __CUDACC__
/**
 * @def SQRTRATIO(a, b)
 *
 * @brief return sqrt(a / b)
 * @detail WARNING: a is twice calculated if is an equation
 */
#define SQRTRATIO(a, b) ((a) * RSQRT((a) * (b)))
#      else//__CUDACC__
#define POW10(a)  (POW(TEN, (a)))
#define  RSQRT(a)    (UNITY / SQRT(a))
#define  RCBRT(a)    (UNITY / CBRT(a))
#define RHYPOT(a, b) (UNITY / HYPOT((a), (b)))
/**
 * @def SQRTRATIO(a, b)
 *
 * @brief return sqrt(a / b)
 */
#define SQRTRATIO(a, b) (SQRT((a) / (b)))
#      endif//__CUDACC__
/**
 * @def IMIN(a, b)
 * return minimum of a and b
 * WARNING: a and b are twice calculated if the variable is an equation
 */
#define IMIN(a, b) (((a) < (b)) ? (a) : (b))
/**
 * @def IMAX(a, b)
 *
 * @brief return maximum of a and b
 * @detail WARNING: a and b are twice calculated if the variable is an equation
 */
#define IMAX(a, b) (((a) > (b)) ? (a) : (b))


#ifdef  __ICC
/* Disable ICC's remark #177: function "hoge" was declared but never referenced */
#     pragma warning (disable:177)
#endif//__ICC

/**
 * @fn pop
 *
 * @brief Evaluate population count (number of contiguous zeros at the head).
 *
 * @param (xx) an inputted number as uint
 * @return population count
 */
static inline uint pop(uint xx)
{
  uint nn = 0;
  while( xx != 0 ){
    nn++;
    xx &= (xx - 1);
  }
  return (nn);
}
/**
 * @fn lpop
 *
 * @brief Evaluate population count (number of contiguous zeros at the head).
 *
 * @param (xx) an inputted number as ulong
 * @return population count
 */
static inline uint lpop(ulong xx)
{
  uint nn = 0;
  while( xx != 0 ){
    nn++;
    xx &= (xx - 1);
  }
  return (nn);
}


/**
 * @fn ilog2
 *
 * @brief Calculate log_2(x) in integer.
 *
 * @param (xx) an inputted number as uint
 * @return log_2(xx) as uint
 *
 * @sa pop
 */
static inline uint ilog2(uint xx)
{
  for(uint ii = 1; ii < sizeof(uint) * CHAR_BIT; ii++)
    xx |= (xx >> ii);
  return (pop(xx) - 1);
}
/**
 * @fn llog2
 *
 * @brief Calculate log_2(x) in integer.
 *
 * @param (xx) an inputted number as ulong
 * @return log_2(xx) as uint
 *
 * @sa lpop
 */
static inline uint llog2(ulong xx)
{
  for(ulong ii = 1; ii < sizeof(ulong) * CHAR_BIT; ii++)
    xx |= (xx >> ii);
  return (lpop(xx) - 1);
}


/**
 * @fn gcd
 *
 * @brief Calculate greatest common divisor
 *
 * @param (mm) an inputted number
 * @param (nn) an inputted number
 * @return greatest common divisor of mm and nn
 */
static inline int gcd(int mm, int nn)
{
  if( mm * nn == 0 )    return (0);

  while( mm != nn ){
    if( mm > nn )      mm -= nn;
    else               nn -= mm;
  }
  return (mm);
}
/**
 * @fn lcm
 *
 * @brief Calculate least common multiple
 *
 * @param (mm) an inputted number
 * @param (nn) an inputted number
 * @return least common multiple of mm and nn
 *
 * @sa gcd
 */
static inline int lcm(int mm, int nn)
{
  if( mm * nn == 0 )    return (0);
  return ((mm * nn) / gcd(mm, nn));
}

#ifdef  __ICC
/* Enable ICC's remark #177: function "hoge" was declared but never referenced */
#     pragma warning (enable:177)
#endif//__ICC


#ifdef  __ICC
/* Disable ICC's remark #1418: external function definition with no prior declaration. */
#     pragma warning (disable:1418)
/* Disable ICC's remark #1419: external declaration in primary source file */
#     pragma warning (disable:1419)
#endif//__ICC


#endif//MACRO_H
