/**
 * @file hdf5lib.h
 *
 * @brief Header file for original macros about HDF5
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef HDF5LIB_H
#define HDF5LIB_H


#include <hdf5.h>


/**
 * @def chkHDF5err(func)
 *
 * @brief wrapper to detect errors related to HDF5
 */
#define chkHDF5err(err) __chkHDF5err(err, __FILE__, __LINE__)


#ifndef H5T_NATIVE_BOOL
#define H5T_NATIVE_BOOL H5T_NATIVE_INT
#endif//H5T_NATIVE_BOOL


/* list of functions appeared in ``hdf5lib.c'' */
#ifdef  __CUDACC__
extern "C"
{
#endif//__CUDACC__
  void __chkHDF5err(herr_t err, const char *file, const int line);
#ifdef  __CUDACC__
}
#endif//__CUDACC__


#endif//HDF5LIB_H
