/**
 * @file constants.c
 *
 * @brief Source code for physical constants and unit system
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2018/02/12 (Mon)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "macro.h"
#include "constants.h"
#include "name.h"


#define UNITSYSTEMFILE "unit.txt"


/** Physical constants in cgs unit  */
real newton;
double boltzmann, protonmass;
const double     newton_cgs = 6.67428e-08;
const double  boltzmann_cgs = 1.3806505e-16;
const double protonmass_cgs = 1.67262171e-24;
const double hubble_dimensionless = 0.673;/**< based on Planck (2013) */
/* const double hubble_dimensionless = 0.6932;/\**< based on WMAP 9 yr w/ BAO w/ eCMB w/ H0 *\/ */

/** Astronomical constants in cgs unit */
const double Msun = 1.9884e+33;
const double   pc = 3.0856775975e+18;
const double  kpc = 3.0856775975e+21;
const double  Mpc = 3.0856775975e+24;
const double   AU = 1.49597871475e+13;
const double Lsun = 3.845e+33;
const double Rsun = 6.9551e+10;
const double   yr = 3.15576e+7;
const double  Myr = 3.15576e+13;
const double  Gyr = 3.15576e+16;

const double    K = 1.0;


/** Constants about state of fluid */
const real mu = ((0.9 + 4.0 * 0.1) / (0.9 + 0.1));/**< mean molecular weight of primordial abundance */
const real gam = (FIVE * ONE_THIRD);/* heat capacity ratio of monoatomic molecule */
/* const real gam = (1.4);/\* heat capacity ratio of diatomic molecule *\/ */
real GamInv, GamMinOne, GamMinOneInv;


/** cosmological constants */
double hubble;/**< Hubble constant */
double rho_crit;/**< critical density */


/** Definition about computational unit */
double unit_mass       , computational_mass_unit;
double unit_length     , computational_length_unit;
double unit_time       , computational_time_unit;
double unit_temperature, computational_temperature_unit;
char        mass_astro_unit_name[CONSTANTS_H_CHAR_WORDS],        mass_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char      length_astro_unit_name[CONSTANTS_H_CHAR_WORDS],      length_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char        time_astro_unit_name[CONSTANTS_H_CHAR_WORDS],        time_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char temperature_astro_unit_name[CONSTANTS_H_CHAR_WORDS], temperature_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];


/**
 * @fn setUnityComputationalUnitSystem
 *
 * @brief set UnityValueSystem
 *
 */
static inline void setUnityComputationalUnitSystem(void)
{
  computational_mass_unit        = 1.0;
  computational_length_unit      = 1.0;
  computational_time_unit        = 1.0;
  computational_temperature_unit = 1.0;

  sprintf(  mass_astro_unit_name4plot, "C.U.");
  sprintf(       mass_astro_unit_name, "C.U.");  unit_mass        = 1.0;
  sprintf(     length_astro_unit_name, "C.U.");  unit_length      = 1.0;
  sprintf(       time_astro_unit_name, "C.U.");  unit_time        = 1.0;
  sprintf(temperature_astro_unit_name, "C.U.");  unit_temperature = 1.0;
}

/**
 * @fn setUnityUnityNewtonConstantUnitSystem
 *
 * @brief set UnityNewton (G = 1)
 *
 */
static inline void setUnityNewtonConstantUnitSystem(void)
{
  computational_mass_unit        = 1.0e+08 * Msun;
  computational_length_unit      = kpc;
  computational_time_unit        = sqrt(computational_length_unit * computational_length_unit / (newton_cgs * computational_mass_unit) * computational_length_unit);
  computational_temperature_unit = 1.0;

  sprintf(mass_astro_unit_name4plot, "#fiM#fr#d%s#u", "#[0x29bf]");
  sprintf(       mass_astro_unit_name, "Msun");  unit_mass        = computational_mass_unit   / Msun;
  sprintf(     length_astro_unit_name, "kpc" );  unit_length      = computational_length_unit / kpc;
  sprintf(       time_astro_unit_name, "C.U.");  unit_time        = 1.0;
  sprintf(temperature_astro_unit_name, "C.U.");  unit_temperature = 1.0;
}

/**
 * @fn setGalacticNucleiUnitSystemCGS
 *
 * @brief set GalacticNucleiScale
 *
 */
static inline void setGalacticNucleiUnitSystemCGS(void)
{
  computational_mass_unit        = 1.0e+08 * Msun;
  computational_length_unit      = 100.0 * pc;
  computational_time_unit        = Myr;
  computational_temperature_unit = K;

  sprintf(mass_astro_unit_name4plot, "#fiM#fr#d%s#u", "#[0x29bf]");
  sprintf(       mass_astro_unit_name, "Msun");  unit_mass        = computational_mass_unit        / Msun;
  sprintf(     length_astro_unit_name, "pc"  );  unit_length      = computational_length_unit      / pc;
  sprintf(       time_astro_unit_name, "Myr" );  unit_time        = computational_time_unit        / Myr;
  sprintf(temperature_astro_unit_name, "K"   );  unit_temperature = computational_temperature_unit / K;
}

/**
 * @fn setGalacticUnitSystemCGS
 *
 * @brief set GalacticScale
 *
 */
static inline void setGalacticUnitSystemCGS(void)
{
  computational_mass_unit        = 1.0e+08 * Msun;
  computational_length_unit      = kpc;
  computational_time_unit        = 100.0 * Myr;
  computational_temperature_unit = K;

  sprintf(mass_astro_unit_name4plot, "#fiM#fr#d%s#u", "#[0x29bf]");
  sprintf(       mass_astro_unit_name, "Msun");  unit_mass        = computational_mass_unit        / Msun;
  sprintf(     length_astro_unit_name, "kpc" );  unit_length      = computational_length_unit      / kpc;
  sprintf(       time_astro_unit_name, "Myr" );  unit_time        = computational_time_unit        / Myr;
  sprintf(temperature_astro_unit_name, "K"   );  unit_temperature = computational_temperature_unit / K;
}

/**
 * @fn setStellarUnitSystemCGS
 *
 * @brief set StellarScale
 *
 */
static inline void setStellarUnitSystemCGS(void)
{
  computational_mass_unit        = Msun;
  computational_length_unit      = AU;
  computational_time_unit        = yr;
  computational_temperature_unit = K;

  sprintf(mass_astro_unit_name4plot, "#fiM#fr#d%s#u", "#[0x29bf]");
  sprintf(       mass_astro_unit_name, "Msun");  unit_mass        = computational_mass_unit        / Msun;
  sprintf(     length_astro_unit_name, "AU"  );  unit_length      = computational_length_unit      / AU;
  sprintf(       time_astro_unit_name, "yr"  );  unit_time        = computational_time_unit        / yr;
  sprintf(temperature_astro_unit_name, "K"   );  unit_temperature = computational_temperature_unit / K;
}

/**
 * @fn setHostGalaxyUnitSystemCGS
 *
 * @brief set HostGalaxyScale
 *
 */
static inline void setHostGalaxyUnitSystemCGS(void)
{
  computational_mass_unit        = 1.0e+10 * Msun;
  computational_length_unit      = 1.0e+1  * kpc;
  computational_time_unit        = 1.0e+2  * Myr;
  computational_temperature_unit = K;

  sprintf(mass_astro_unit_name4plot, "#fiM#fr#d%s#u", "#[0x29bf]");
  sprintf(       mass_astro_unit_name, "Msun");  unit_mass        = computational_mass_unit        / Msun;
  sprintf(     length_astro_unit_name, "kpc" );  unit_length      = computational_length_unit      / kpc;
  sprintf(       time_astro_unit_name, "Myr" );  unit_time        = computational_time_unit        / Myr;
  sprintf(temperature_astro_unit_name, "K"   );  unit_temperature = computational_temperature_unit / K;
}

/**
 * @fn setGalactICSUnitSystemCGS
 *
 * @brief set GalactICSunit
 *
 */
static inline void setGalactICSUnitSystemCGS(void)
{
  computational_length_unit      = kpc;
  computational_time_unit        = computational_length_unit / 1.0e+7;  /**< unit of velocity is 100 km/s = 10^7 cm/s */
  computational_mass_unit        = (computational_length_unit / newton_cgs) * 1.0e+14;  /**< 1.0e+14 is (computational_length_unit / computational_time_unit)^2 */
  computational_temperature_unit = 1.0;

  sprintf(mass_astro_unit_name4plot, "#fiM#fr#d%s#u", "#[0x29bf]");
  sprintf(       mass_astro_unit_name, "Msun");  unit_mass        = computational_mass_unit   / Msun;
  sprintf(     length_astro_unit_name, "kpc" );  unit_length      = computational_length_unit / kpc;
  sprintf(       time_astro_unit_name, "Myr" );  unit_time        = computational_time_unit   / Myr;
  sprintf(temperature_astro_unit_name, "C.U.");  unit_temperature = 1.0;
}


/**
 * @fn copyFundamentalUnitName
 *
 * @brief copy unit name for PLplot
 *
 */
static inline void copyFundamentalUnitName(void)
{
  sprintf(     length_astro_unit_name4plot, "%s",      length_astro_unit_name);
  sprintf(       time_astro_unit_name4plot, "%s",        time_astro_unit_name);
  sprintf(temperature_astro_unit_name4plot, "%s", temperature_astro_unit_name);
}


/** Conversion factor about unit transformation */
/** computational ==> cgs */
double        mass2cgs;
double      length2cgs;
double        time2cgs;
double temperature2cgs;
double     density2cgs;
double col_density2cgs;
double    ndensity2cgs;
double colndensity2cgs;
double    velocity2cgs;
double       accel2cgs;
double      energy2cgs;
double     senergy2cgs;
double    pressure2cgs;

/**
 * @fn calcConversionFactorFromComputationalToCGS
 *
 * @brief calculate conversion factor from computational unit to CGS
 *
 */
static inline void calcConversionFactorFromComputationalToCGS(void)
{
  mass2cgs        = computational_mass_unit;
  length2cgs      = computational_length_unit;
  time2cgs        = computational_time_unit;
  temperature2cgs = computational_temperature_unit;

  density2cgs     = mass2cgs / (length2cgs * length2cgs * length2cgs);
  col_density2cgs = mass2cgs / (length2cgs * length2cgs);
  ndensity2cgs    =     density2cgs / ((double)mu * protonmass_cgs);
  colndensity2cgs = col_density2cgs / ((double)mu * protonmass_cgs);
  velocity2cgs    = length2cgs / time2cgs;
  accel2cgs       = velocity2cgs / time2cgs;
  energy2cgs      = mass2cgs * velocity2cgs * velocity2cgs;
  senergy2cgs     =            velocity2cgs * velocity2cgs;
  pressure2cgs    = energy2cgs / (length2cgs * length2cgs * length2cgs);
}


/** cgs ==> computational */
double        mass2com;
double      length2com;
double        time2com;
double temperature2com;
double     density2com;
double col_density2com;
double    ndensity2com;
double colndensity2com;
double    velocity2com;
double       accel2com;
double      energy2com;
double     senergy2com;
double    pressure2com;

/**
 * @fn calcConversionFactorFromCGSToComputational
 *
 * @brief calculate conversion factor from CGS to computational unit
 *
 */
static inline void calcConversionFactorFromCGSToComputational(void)
{
  mass2com        = 1.0 /        mass2cgs;
  length2com      = 1.0 /      length2cgs;
  time2com        = 1.0 /        time2cgs;
  temperature2com = 1.0 / temperature2cgs;
  density2com     = 1.0 /     density2cgs;
  col_density2com = 1.0 / col_density2cgs;
  ndensity2com    = 1.0 /    ndensity2cgs;
  colndensity2com = 1.0 / colndensity2cgs;
  velocity2com    = 1.0 /    velocity2cgs;
  accel2com       = 1.0 /       accel2cgs;
  energy2com      = 1.0 /      energy2cgs;
  senergy2com     = 1.0 /     senergy2cgs;
  pressure2com    = 1.0 /    pressure2cgs;
}


/** computational ==> astro */
double        mass2astro;
double      length2astro;
double        time2astro;
double temperature2astro;
double     density2astro;
double    ndensity2astro;
double col_density2astro;
double colndensity2astro;
double    velocity2astro;
double       accel2astro;
double      energy2astro;
double     senergy2astro;
double    pressure2astro;
char     density_astro_unit_name[CONSTANTS_H_CHAR_WORDS],     density_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char    ndensity_astro_unit_name[CONSTANTS_H_CHAR_WORDS],    ndensity_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char col_density_astro_unit_name[CONSTANTS_H_CHAR_WORDS], col_density_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char colndensity_astro_unit_name[CONSTANTS_H_CHAR_WORDS], colndensity_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char    velocity_astro_unit_name[CONSTANTS_H_CHAR_WORDS],    velocity_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char       accel_astro_unit_name[CONSTANTS_H_CHAR_WORDS],       accel_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char      energy_astro_unit_name[CONSTANTS_H_CHAR_WORDS],      energy_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char     senergy_astro_unit_name[CONSTANTS_H_CHAR_WORDS],     senergy_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];
char    pressure_astro_unit_name[CONSTANTS_H_CHAR_WORDS],    pressure_astro_unit_name4plot[CONSTANTS_H_PLOT_WORDS];

/**
 * @fn calcConversionFactorFromComputationalToGalacticNucleiScaleUnit
 *
 * @brief calculate conversion factor from computational unit to astrophysical unit
 *
 */
static inline void calcConversionFactorFromComputationalToGalacticNucleiScaleUnit(void)
{
  mass2astro        = unit_mass;
  length2astro      = unit_length;
  time2astro        = unit_time;
  temperature2astro = unit_temperature;
  density2astro     =     density2cgs * 1.0e+20;  /**< unit of density is 10^-20 g/cm3 */
  ndensity2astro    =    ndensity2cgs;
  col_density2astro = col_density2cgs;
  colndensity2astro = colndensity2cgs;
  velocity2astro    =    velocity2cgs * 1.0e-5 ;  /**< unit of velocity is km/s = 10^5 cm/s */
  energy2astro      =      energy2cgs * 1.0e-50;  /**< unit of energy is 10^50 erg */
  senergy2astro     =     senergy2cgs * 1.0e-10;  /**< unit of specific energy is 10^10 erg/g */
  pressure2astro    =    pressure2cgs * 1.0e+10;  /**< unit of pressure is 10^-10 erg/cm3 */
  accel2astro       = velocity2astro / time2astro;

  sprintf(    density_astro_unit_name, "E-20 g / cm3");
  sprintf(   ndensity_astro_unit_name, "/ cm3");
  sprintf(col_density_astro_unit_name, "g / cm2");
  sprintf(colndensity_astro_unit_name, "/ cm2");
  sprintf(   velocity_astro_unit_name, "km / s");
  sprintf(     energy_astro_unit_name, "E+50 erg");
  sprintf(    senergy_astro_unit_name, "E+10 erg / g");
  sprintf(   pressure_astro_unit_name, "E-10 erg / cm3");
  sprintf(      accel_astro_unit_name, "%s / %s", velocity_astro_unit_name, time_astro_unit_name);

  sprintf(    density_astro_unit_name4plot, "10#u-20#d g cm#u-3#d");
  sprintf(   ndensity_astro_unit_name4plot, "cm#u-3#d");
  sprintf(col_density_astro_unit_name4plot, "g cm#u-2#d");
  sprintf(colndensity_astro_unit_name4plot, "cm#u-2#d");
  sprintf(   velocity_astro_unit_name4plot, "km s#u-1#d");
  sprintf(     energy_astro_unit_name4plot, "10#u50#d erg");
  sprintf(    senergy_astro_unit_name4plot, "10#u10#d erg g#u-1#d");
  sprintf(   pressure_astro_unit_name4plot, "10#u-10#d erg cm#u-3#d");
  sprintf(      accel_astro_unit_name4plot, "%s %s#u-1#d", velocity_astro_unit_name, time_astro_unit_name);
}

/**
 * @fn calcConversionFactorFromComputationalToGalacticScaleUnit
 *
 * @brief calculate conversion factor from computational unit to astrophysical unit
 *
 */
static inline void calcConversionFactorFromComputationalToGalacticScaleUnit(void)
{
  mass2astro        = unit_mass;
  length2astro      = unit_length;
  time2astro        = unit_time;
  temperature2astro = unit_temperature;
  density2astro     =     density2cgs * 1.0e+20;  /**< unit of density is 10^-20 g/cm3 */
  ndensity2astro    =    ndensity2cgs;
  col_density2astro = col_density2cgs;
  colndensity2astro = colndensity2cgs;
  velocity2astro    =    velocity2cgs * 1.0e-5 ;  /**< unit of velocity is km/s = 10^5 cm/s */
  energy2astro      =      energy2cgs * 1.0e-50;  /**< unit of energy is 10^50 erg */
  senergy2astro     =     senergy2cgs * 1.0e-10;  /**< unit of specific energy is 10^10 erg/g */
  pressure2astro    =    pressure2cgs * 1.0e+10;  /**< unit of pressure is 10^-10 erg/cm3 */
  accel2astro       = velocity2astro / time2astro;

  sprintf(    density_astro_unit_name, "E-20 g / cm3");
  sprintf(   ndensity_astro_unit_name, "/ cm3");
  sprintf(col_density_astro_unit_name, "g / cm2");
  sprintf(colndensity_astro_unit_name, "/ cm2");
  sprintf(   velocity_astro_unit_name, "km / s");
  sprintf(     energy_astro_unit_name, "E+50 erg");
  sprintf(    senergy_astro_unit_name, "E+10 erg / g");
  sprintf(   pressure_astro_unit_name, "E-10 erg / cm3");
  sprintf(      accel_astro_unit_name, "%s / %s", velocity_astro_unit_name, time_astro_unit_name);

  sprintf(    density_astro_unit_name4plot, "10#u-20#d g cm#u-3#d");
  sprintf(   ndensity_astro_unit_name4plot, "cm#u-3#d");
  sprintf(col_density_astro_unit_name4plot, "g cm#u-2#d");
  sprintf(colndensity_astro_unit_name4plot, "cm#u-2#d");
  sprintf(   velocity_astro_unit_name4plot, "km s#u-1#d");
  sprintf(     energy_astro_unit_name4plot, "10#u50#d erg");
  sprintf(    senergy_astro_unit_name4plot, "10#u10#d erg g#u-1#d");
  sprintf(   pressure_astro_unit_name4plot, "10#u-10#d erg cm#u-3#d");
  sprintf(      accel_astro_unit_name4plot, "%s %s#u-1#d", velocity_astro_unit_name, time_astro_unit_name);
}

/**
 * @fn calcConversionFactorFromComputationalToStellarScaleUnit
 *
 * @brief calculate conversion factor from computational unit to astrophysical unit
 *
 */
static inline void calcConversionFactorFromComputationalToStellarScaleUnit(void)
{
  mass2astro        = unit_mass;
  length2astro      = unit_length;
  time2astro        = unit_time;
  temperature2astro = unit_temperature;
  density2astro     =     density2cgs * 1.0e+10;  /**< unit of density is 10^-10 g/cm3 */
  ndensity2astro    =    ndensity2cgs * 1.0e-10;  /**< unit of ndensity is 10^10 /cm3 */
  col_density2astro = col_density2cgs;
  colndensity2astro = colndensity2cgs;
  velocity2astro    =    velocity2cgs * 1.0e-5 ;  /**< unit of velocity is km/s = 10^5 cm/s */
  energy2astro      =      energy2cgs * 1.0e-40;  /**< unit of energy is 10^40 erg */
  senergy2astro     =     senergy2cgs * 1.0e-10;  /**< unit of specific energy is 10^10 erg/g */
  pressure2astro    =    pressure2cgs;
  accel2astro       = velocity2astro / time2astro;

  sprintf(    density_astro_unit_name, "E-10 g / cm3");
  sprintf(   ndensity_astro_unit_name, "E+10 / cm3");
  sprintf(col_density_astro_unit_name, "g / cm2");
  sprintf(colndensity_astro_unit_name, "/ cm2");
  sprintf(   velocity_astro_unit_name, "km / s");
  sprintf(     energy_astro_unit_name, "E+40 erg");
  sprintf(    senergy_astro_unit_name, "E+10 erg / g");
  sprintf(   pressure_astro_unit_name, "erg / cm3");
  sprintf(      accel_astro_unit_name, "%s / %s", velocity_astro_unit_name, time_astro_unit_name);

  sprintf(    density_astro_unit_name4plot, "10#u-10#d g cm#u-3#d");
  sprintf(   ndensity_astro_unit_name4plot, "10#u10#d cm#u-3#d");
  sprintf(col_density_astro_unit_name4plot, "g cm#u-2#d");
  sprintf(colndensity_astro_unit_name4plot, "cm#u-2#d");
  sprintf(   velocity_astro_unit_name4plot, "km s#u-1#d");
  sprintf(     energy_astro_unit_name4plot, "10#u40#d erg");
  sprintf(    senergy_astro_unit_name4plot, "10#u10#d erg g#u-1#d");
  sprintf(   pressure_astro_unit_name4plot, "erg cm#u-3#d");
  sprintf(      accel_astro_unit_name4plot, "%s %s#u-1#d", velocity_astro_unit_name, time_astro_unit_name);
}

/**
 * @fn calcConversionFactorFromComputationalToHostGalaxyScaleUnit
 *
 * @brief calculate conversion factor from computational unit to astrophysical unit
 *
 */
static inline void calcConversionFactorFromComputationalToHostGalaxyScaleUnit(void)
{
  mass2astro        = unit_mass;
  length2astro      = unit_length;
  time2astro        = unit_time;
  temperature2astro = unit_temperature;
  density2astro     =     density2cgs * 1.0e+20;  /**< unit of density is 10^-20 g/cm3 */
  ndensity2astro    =    ndensity2cgs;
  col_density2astro = col_density2cgs;
  colndensity2astro = colndensity2cgs;
  velocity2astro    =    velocity2cgs * 1.0e-5 ;  /**< unit of velocity is km/s = 10^5 cm/s */
  energy2astro      =      energy2cgs * 1.0e-50;  /**< unit of energy is 10^50 erg */
  senergy2astro     =     senergy2cgs * 1.0e-10;  /**< unit of specific energy is 10^10 erg/g */
  pressure2astro    =    pressure2cgs * 1.0e+10;  /**< unit of pressure is 10^-10 erg/cm3 */
  accel2astro       = velocity2astro / time2astro;

  sprintf(    density_astro_unit_name, "E-20 g / cm3");
  sprintf(   ndensity_astro_unit_name, "/ cm3");
  sprintf(col_density_astro_unit_name, "g / cm2");
  sprintf(colndensity_astro_unit_name, "/ cm2");
  sprintf(   velocity_astro_unit_name, "km / s");
  sprintf(     energy_astro_unit_name, "E+50 erg");
  sprintf(    senergy_astro_unit_name, "E+10 erg / g");
  sprintf(   pressure_astro_unit_name, "E-10 erg / cm3");
  sprintf(      accel_astro_unit_name, "%s / %s", velocity_astro_unit_name, time_astro_unit_name);

  sprintf(    density_astro_unit_name4plot, "10#u-20#d g cm#u-3#d");
  sprintf(   ndensity_astro_unit_name4plot, "cm#u-3#d");
  sprintf(col_density_astro_unit_name4plot, "g cm#u-2#d");
  sprintf(colndensity_astro_unit_name4plot, "cm#u-2#d");
  sprintf(   velocity_astro_unit_name4plot, "km s#u-1#d");
  sprintf(     energy_astro_unit_name4plot, "10#u50#d erg");
  sprintf(    senergy_astro_unit_name4plot, "10#u10#d erg g#u-1#d");
  sprintf(   pressure_astro_unit_name4plot, "10#u-10#d erg cm#u-3#d");
  sprintf(      accel_astro_unit_name4plot, "%s %s#u-1#d", velocity_astro_unit_name, time_astro_unit_name);
}

/**
 * @fn calcConversionFactorFromComputationalToUnityNewtonConstantUnit
 *
 * @brief calculate conversion factor from computational unit to astrophysical unit
 *
 */
static inline void calcConversionFactorFromComputationalToUnityNewtonConstantUnit(void)
{
  mass2astro        = unit_mass;
  length2astro      = unit_length;
  time2astro        = unit_time;
  temperature2astro = unit_temperature;
  density2astro     =     density2cgs * 1.0e+20;  /**< unit of density is 10^-20 g/cm3 */
  ndensity2astro    =    ndensity2cgs;
  col_density2astro = col_density2cgs;
  colndensity2astro = colndensity2cgs;
  velocity2astro    =    velocity2cgs * 1.0e-5 ;  /**< unit of velocity is km/s = 10^5 cm/s */
  energy2astro      =      energy2cgs * 1.0e-50;  /**< unit of energy is 10^50 erg */
  senergy2astro     =     senergy2cgs * 1.0e-10;  /**< unit of specific energy is 10^10 erg/g */
  pressure2astro    =    pressure2cgs * 1.0e+10;  /**< unit of pressure is 10^-10 erg/cm3 */
  accel2astro       = velocity2astro / time2astro;

  sprintf(    density_astro_unit_name, "E-20 g / cm3");
  sprintf(   ndensity_astro_unit_name, "/ cm3");
  sprintf(col_density_astro_unit_name, "g / cm2");
  sprintf(colndensity_astro_unit_name, "/ cm2");
  sprintf(   velocity_astro_unit_name, "km / s");
  sprintf(     energy_astro_unit_name, "E+50 erg");
  sprintf(    senergy_astro_unit_name, "E+10 erg / g");
  sprintf(   pressure_astro_unit_name, "E-10 erg / cm3");
  sprintf(      accel_astro_unit_name, "%s / %s", velocity_astro_unit_name, time_astro_unit_name);

  sprintf(    density_astro_unit_name4plot, "10#u-20#d g cm#u-3#d");
  sprintf(   ndensity_astro_unit_name4plot, "cm#u-3#d");
  sprintf(col_density_astro_unit_name4plot, "g cm#u-2#d");
  sprintf(colndensity_astro_unit_name4plot, "cm#u-2#d");
  sprintf(   velocity_astro_unit_name4plot, "km s#u-1#d");
  sprintf(     energy_astro_unit_name4plot, "10#u50#d erg");
  sprintf(    senergy_astro_unit_name4plot, "10#u10#d erg g#u-1#d");
  sprintf(   pressure_astro_unit_name4plot, "10#u-10#d erg cm#u-3#d");
  sprintf(      accel_astro_unit_name4plot, "%s %s#u-1#d", velocity_astro_unit_name, time_astro_unit_name);
}

/**
 * @fn setUnityConversionFactor
 *
 * @brief calculate conversion factor from computational unit to astrophysical unit
 *
 */
static inline void setUnityConversionFactor(void)
{
  mass2astro        = 1.0;
  length2astro      = 1.0;
  time2astro        = 1.0;
  temperature2astro = 1.0;
  density2astro     = 1.0;
  ndensity2astro    = 1.0;
  col_density2astro = 1.0;
  colndensity2astro = 1.0;
  velocity2astro    = 1.0;
  energy2astro      = 1.0;
  senergy2astro     = 1.0;
  pressure2astro    = 1.0;
  accel2astro       = 1.0;

  sprintf(    density_astro_unit_name, "C.U.");
  sprintf(   ndensity_astro_unit_name, "C.U.");
  sprintf(col_density_astro_unit_name, "C.U.");
  sprintf(colndensity_astro_unit_name, "C.U.");
  sprintf(   velocity_astro_unit_name, "C.U.");
  sprintf(     energy_astro_unit_name, "C.U.");
  sprintf(    senergy_astro_unit_name, "C.U.");
  sprintf(   pressure_astro_unit_name, "C.U.");
  sprintf(      accel_astro_unit_name, "C.U.");

  sprintf(    density_astro_unit_name4plot, "C.U.");
  sprintf(   ndensity_astro_unit_name4plot, "C.U.");
  sprintf(col_density_astro_unit_name4plot, "C.U.");
  sprintf(colndensity_astro_unit_name4plot, "C.U.");
  sprintf(   velocity_astro_unit_name4plot, "C.U.");
  sprintf(     energy_astro_unit_name4plot, "C.U.");
  sprintf(    senergy_astro_unit_name4plot, "C.U.");
  sprintf(   pressure_astro_unit_name4plot, "C.U.");
  sprintf(      accel_astro_unit_name4plot, "C.U.");
}

/**
 * @fn calcConversionFactorFromComputationalToGalactICSUnit
 *
 * @brief calculate conversion factor from computational unit to astrophysical unit
 *
 */
static inline void calcConversionFactorFromComputationalToGalactICSUnit(void)
{
  mass2astro        = unit_mass;
  length2astro      = unit_length;
  time2astro        = unit_time;
  temperature2astro = unit_temperature;
  density2astro     =     density2cgs * 1.0e+20;  /**< unit of density is 10^-20 g/cm3 */
  ndensity2astro    =    ndensity2cgs;
  col_density2astro = col_density2cgs;
  colndensity2astro = colndensity2cgs;
  velocity2astro    =    velocity2cgs * 1.0e-5 ;  /**< unit of velocity is km/s = 10^5 cm/s */
  energy2astro      =      energy2cgs * 1.0e-50;  /**< unit of energy is 10^50 erg */
  senergy2astro     =     senergy2cgs * 1.0e-10;  /**< unit of specific energy is 10^10 erg/g */
  pressure2astro    =    pressure2cgs * 1.0e+10;  /**< unit of pressure is 10^-10 erg/cm3 */
  accel2astro       = velocity2astro / time2astro;

  sprintf(    density_astro_unit_name, "E-20 g / cm3");
  sprintf(   ndensity_astro_unit_name, "/ cm3");
  sprintf(col_density_astro_unit_name, "g / cm2");
  sprintf(colndensity_astro_unit_name, "/ cm2");
  sprintf(   velocity_astro_unit_name, "km / s");
  sprintf(     energy_astro_unit_name, "E+50 erg");
  sprintf(    senergy_astro_unit_name, "E+10 erg / g");
  sprintf(   pressure_astro_unit_name, "E-10 erg / cm3");
  sprintf(      accel_astro_unit_name, "%s / %s", velocity_astro_unit_name, time_astro_unit_name);

  sprintf(    density_astro_unit_name4plot, "10#u-20#d g cm#u-3#d");
  sprintf(   ndensity_astro_unit_name4plot, "cm#u-3#d");
  sprintf(col_density_astro_unit_name4plot, "g cm#u-2#d");
  sprintf(colndensity_astro_unit_name4plot, "cm#u-2#d");
  sprintf(   velocity_astro_unit_name4plot, "km s#u-1#d");
  sprintf(     energy_astro_unit_name4plot, "10#u50#d erg");
  sprintf(    senergy_astro_unit_name4plot, "10#u10#d erg g#u-1#d");
  sprintf(   pressure_astro_unit_name4plot, "10#u-10#d erg cm#u-3#d");
  sprintf(      accel_astro_unit_name4plot, "%s %s#u-1#d", velocity_astro_unit_name, time_astro_unit_name);
}


/** astro ==> computational */
double        mass_astro2com;
double      length_astro2com;
double        time_astro2com;
double temperature_astro2com;
double     density_astro2com;
double    ndensity_astro2com;
double col_density_astro2com;
double colndensity_astro2com;
double    velocity_astro2com;
double       accel_astro2com;
double      energy_astro2com;
double     senergy_astro2com;
double    pressure_astro2com;

/**
 * @fn calcConversionFactorFromAstroToComputational
 *
 * @brief calculate conversion factor from astrophysical unit to computational unit
 *
 */
static inline void calcConversionFactorFromAstroToComputational(void)
{
  mass_astro2com        = 1.0 / mass2astro;
  length_astro2com      = 1.0 / length2astro;
  time_astro2com        = 1.0 / time2astro;
  temperature_astro2com = 1.0 / temperature2astro;
  density_astro2com     = 1.0 / density2astro;
  ndensity_astro2com    = 1.0 / ndensity2astro;
  col_density_astro2com = 1.0 / col_density2astro;
  colndensity_astro2com = 1.0 / colndensity2astro;
  velocity_astro2com    = 1.0 / velocity2astro;
  accel_astro2com       = 1.0 / accel2astro;
  energy_astro2com      = 1.0 / energy2astro;
  senergy_astro2com     = 1.0 / senergy2astro;
  pressure_astro2com    = 1.0 / pressure2astro;
}


/**
 * @fn convertUnitSystemOfPhysicalConstantsFromCGSToComputational
 *
 * @brief convert physical constants from CGS to computational unit
 *
 */
static inline void convertUnitSystemOfPhysicalConstantsFromCGSToComputational(void)
{
  newton     = (real)(newton_cgs * length2com * velocity2com * velocity2com / mass2com);
  boltzmann  = boltzmann_cgs * energy2com / temperature2com;
  protonmass = protonmass_cgs * mass2com;
}


/**
 * @fn setPhysicalConstantsAndUnitSystem
 *
 * @brief Set physical constants in the specified unit system
 *
 * @param (UnitSystem) the specified unit system
 * @param (print) print out the result
 */
void setPhysicalConstantsAndUnitSystem(const int UnitSystem, const int print)
{
  char filename[256];
  FILE *fp;

  /** set cosmological constants */
  hubble  = 100.0 * hubble_dimensionless;/**< in units of km s^-1 Mpc^-1 */
  hubble *= 1.0e+5 / Mpc;/**< in units of CGS */
  rho_crit = 3.0 * hubble * hubble / (8.0 * M_PI * newton_cgs);


  if( print ){
    sprintf(filename, "%s/%s", DOCUMENTFOLDER, UNITSYSTEMFILE);
    fp = fopen(filename, "w");
    if( fp == NULL ){      __KILL__(stderr, "%s\n", "ERROR: UNITSYSTEMFILE couldn't open.");    }

    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Physical Constants in CGS unit are tabulated below:\n");
    fprintf(fp, " G  = %e cm^3 /(g s^2)\n", newton_cgs);
    fprintf(fp, "k_B = %e erg/K\n",         boltzmann_cgs);
    fprintf(fp, "m_p = %e g\n",             protonmass_cgs);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Astrophysical Constants in CGS unit are tabulated below:\n");
    fprintf(fp, "Msun = %e g\n",     Msun);
    fprintf(fp, "Lsun = %e erg/s\n", Lsun);
    fprintf(fp, "Rsun = %e cm\n",    Rsun);
    fprintf(fp, "  pc = %e cm\n",  pc);
    fprintf(fp, " kpc = %e cm\n", kpc);
    fprintf(fp, " Mpc = %e cm\n", Mpc);
    fprintf(fp, "  AU = %e cm\n",  AU);
    fprintf(fp, "  yr = %e s\n",  yr);
    fprintf(fp, " Myr = %e s\n", Myr);
    fprintf(fp, " Gyr = %e s\n", Gyr);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Constants about State of Fluid are tabulated below:\n");
    fprintf(fp, "mean molecular weight = %f\n", mu);
    fprintf(fp, "heat capacity ratio   = %f\n", gam);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Cosmological parameters in CGS units are tabulated below:\n");
    fprintf(fp, "       h = %e\n", hubble_dimensionless);
    fprintf(fp, "     H_0 = %e sec^-1\n", hubble);
    fprintf(fp, "rho_crit = %e g cm^-3\n", rho_crit);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Useful conversion factors are tabulated below:\n");
    fprintf(fp, "Msun pc^-3 = %e    g cm^-3\n", Msun / (pc * pc * pc));
    fprintf(fp, "   g cm^-3 = %e Msun pc^-3\n", pc * pc * pc / Msun);
    fprintf(fp, "Msun pc^-2 = %e    g cm^-2\n", Msun / (pc * pc));
    fprintf(fp, "   g cm^-2 = %e Msun pc^-2\n", pc * pc / Msun);
    fprintf(fp, " pc Myr^-1 = %e km   s^-1\n", pc / Myr * 1.0e-5);
    fprintf(fp, " km   s^-1 = %e pc Myr^-1\n", Myr / pc * 1.0e+5);
    fprintf(fp, "#####################################################################################\n");
  }


  GamInv       = UNITY / gam;
  GamMinOne    = gam - UNITY;
  GamMinOneInv = UNITY / GamMinOne;


  switch( UnitSystem ){
  case  GalacticNucleiScale:    setGalacticNucleiUnitSystemCGS()  ;    break;
  case  GalacticScale      :    setGalacticUnitSystemCGS()        ;    break;
  case  StellarScale       :    setStellarUnitSystemCGS()         ;    break;
  case  UnityNewton        :    setUnityNewtonConstantUnitSystem();    break;
  case  HostGalaxyScale    :    setHostGalaxyUnitSystemCGS()      ;    break;
  case  GalactICSunit      :    setGalactICSUnitSystemCGS()       ;    break;
  default                  :    setUnityComputationalUnitSystem() ;    break;
  }

  copyFundamentalUnitName();

  calcConversionFactorFromComputationalToCGS();
  calcConversionFactorFromCGSToComputational();

  switch( UnitSystem ){
  case  GalacticNucleiScale:    calcConversionFactorFromComputationalToGalacticNucleiScaleUnit();    break;
  case  GalacticScale      :    calcConversionFactorFromComputationalToGalacticScaleUnit()      ;    break;
  case  StellarScale       :    calcConversionFactorFromComputationalToStellarScaleUnit()       ;    break;
  case  UnityNewton        :    calcConversionFactorFromComputationalToUnityNewtonConstantUnit();    break;
  case  HostGalaxyScale    :    calcConversionFactorFromComputationalToHostGalaxyScaleUnit()    ;    break;
  case  GalactICSunit      :    calcConversionFactorFromComputationalToGalactICSUnit()          ;    break;
  default                  :    setUnityConversionFactor()                                      ;    break;
  }

  convertUnitSystemOfPhysicalConstantsFromCGSToComputational();

  /** set cosmological constants */
  hubble /= time2com;  /**< H0 = 100.0 * hubble_dimensionless * 1.0e+5 / (time2astro * Mpc);/\* H0 = 100 h km s^-1 Mpc^-1 *\/ */
  rho_crit *= density2com;  /**< rho_crit = 3.0 * H0 * H0 / (8.0 * M_PI * (double)newton); */

  calcConversionFactorFromAstroToComputational();


  if( print ){
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Computational Unit system which applied is\n\t");
  }


  switch( UnitSystem ){
  case  GalacticNucleiScale:    if( print )      fprintf(fp, "for Galactic Nuclei Scale\n"                           );    break;
  case  GalacticScale      :    if( print )      fprintf(fp, "for Galactic Scale\n"                                  );    break;
  case  StellarScale       :    if( print )      fprintf(fp, "for Stellar Scale\n"                                   );    break;
  case  UnityNewton        :    if( print )      fprintf(fp, "for unity G w/ Galactic Scale for N-body simulations\n");    break;
  case  HostGalaxyScale    :    if( print )      fprintf(fp, "for Host Galaxy Scale\n"                               );    break;
  case  GalactICSunit      :    if( print )      fprintf(fp, "the same unit system with GalactICS\n"                 );    break;
  default:    if( print )      fprintf(fp, "Unity\n");
    newton = UNITY;
    boltzmann = 1.0;
    protonmass = 1.0;
    break;
  }/* switch( UnitSystem ){ */

  if( print ){
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Definition of Computational Unit are tabulated below:\n");
    fprintf(fp, "       mass unit = %e (%s)\n", unit_mass       ,        mass_astro_unit_name);
    fprintf(fp, "     length unit = %e (%s)\n", unit_length     ,      length_astro_unit_name);
    fprintf(fp, "       time unit = %e (%s)\n", unit_time       ,        time_astro_unit_name);
    fprintf(fp, "temperature unit = %e (%s)\n", unit_temperature, temperature_astro_unit_name);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Corresponding Computational Unit are tabulated below:\n");
    fprintf(fp, "       velocity: 1 C.U. = %e (%s)\n",    velocity2astro,    velocity_astro_unit_name);
    fprintf(fp, "   acceleration: 1 C.U. = %e (%s)\n",       accel2astro,       accel_astro_unit_name);
    fprintf(fp, "        density: 1 C.U. = %e (%s)\n",     density2astro,     density_astro_unit_name);
    fprintf(fp, " number density: 1 C.U. = %e (%s)\n",    ndensity2astro,    ndensity_astro_unit_name);
    fprintf(fp, " column density: 1 C.U. = %e (%s)\n", col_density2astro, col_density_astro_unit_name);
    fprintf(fp, "         energy: 1 C.U. = %e (%s)\n",      energy2astro,      energy_astro_unit_name);
    fprintf(fp, "specific energy: 1 C.U. = %e (%s)\n",     senergy2astro,     senergy_astro_unit_name);
    fprintf(fp, "       pressure: 1 C.U. = %e (%s)\n",    pressure2astro,    pressure_astro_unit_name);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Physical Constants in Computational unit are tabulated below:\n");
    fprintf(fp, " G  = %e\n", newton);
    fprintf(fp, "k_B = %e\n", boltzmann);
    fprintf(fp, "m_p = %e\n", protonmass);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Conversion Factor from Astrophysical Unit to Computational Unit are tabulated below:\n");
    fprintf(fp, "       mass_astro2com = %e\n",        mass_astro2com);
    fprintf(fp, "     length_astro2com = %e\n",      length_astro2com);
    fprintf(fp, "       time_astro2com = %e\n",        time_astro2com);
    fprintf(fp, "temperature_astro2com = %e\n", temperature_astro2com);
    fprintf(fp, "    density_astro2com = %e\n",     density_astro2com);
    fprintf(fp, "   ndensity_astro2com = %e\n",    ndensity_astro2com);
    fprintf(fp, "col_density_astro2com = %e\n", col_density_astro2com);
    fprintf(fp, "colndensity_astro2com = %e\n", colndensity_astro2com);
    fprintf(fp, "   velocity_astro2com = %e\n",    velocity_astro2com);
    fprintf(fp, "      accel_astro2com = %e\n",       accel_astro2com);
    fprintf(fp, "     energy_astro2com = %e\n",      energy_astro2com);
    fprintf(fp, "    senergy_astro2com = %e\n",     senergy_astro2com);
    fprintf(fp, "   pressure_astro2com = %e\n",    pressure_astro2com);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Conversion Factor from Computational Unit to Astrophysical Unit are tabulated below:\n");
    fprintf(fp, "       mass2astro = %e\n",        mass2astro);
    fprintf(fp, "     length2astro = %e\n",      length2astro);
    fprintf(fp, "       time2astro = %e\n",        time2astro);
    fprintf(fp, "temperature2astro = %e\n", temperature2astro);
    fprintf(fp, "    density2astro = %e\n",     density2astro);
    fprintf(fp, "   ndensity2astro = %e\n",    ndensity2astro);
    fprintf(fp, "col_density2astro = %e\n", col_density2astro);
    fprintf(fp, "colndensity2astro = %e\n", colndensity2astro);
    fprintf(fp, "   velocity2astro = %e\n",    velocity2astro);
    fprintf(fp, "      accel2astro = %e\n",       accel2astro);
    fprintf(fp, "     energy2astro = %e\n",      energy2astro);
    fprintf(fp, "    senergy2astro = %e\n",     senergy2astro);
    fprintf(fp, "   pressure2astro = %e\n",    pressure2astro);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Conversion Factor from Computational Unit to CGS are tabulated below:\n");
    fprintf(fp, "       mass2cgs = %e\n",        mass2cgs);
    fprintf(fp, "     length2cgs = %e\n",      length2cgs);
    fprintf(fp, "       time2cgs = %e\n",        time2cgs);
    fprintf(fp, "temperature2cgs = %e\n", temperature2cgs);
    fprintf(fp, "    density2cgs = %e\n",     density2cgs);
    fprintf(fp, "   ndensity2cgs = %e\n",    ndensity2cgs);
    fprintf(fp, "col_density2cgs = %e\n", col_density2cgs);
    fprintf(fp, "colndensity2cgs = %e\n", colndensity2cgs);
    fprintf(fp, "   velocity2cgs = %e\n",    velocity2cgs);
    fprintf(fp, "      accel2cgs = %e\n",       accel2cgs);
    fprintf(fp, "     energy2cgs = %e\n",      energy2cgs);
    fprintf(fp, "    senergy2cgs = %e\n",     senergy2cgs);
    fprintf(fp, "   pressure2cgs = %e\n",    pressure2cgs);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Conversion Factor from CGS to Computational Unit are tabulated below:\n");
    fprintf(fp, "       mass2com = %e\n",        mass2com);
    fprintf(fp, "     length2com = %e\n",      length2com);
    fprintf(fp, "       time2com = %e\n",        time2com);
    fprintf(fp, "temperature2com = %e\n", temperature2com);
    fprintf(fp, "    density2com = %e\n",     density2com);
    fprintf(fp, "   ndensity2com = %e\n",    ndensity2com);
    fprintf(fp, "col_density2com = %e\n", col_density2com);
    fprintf(fp, "colndensity2com = %e\n", colndensity2com);
    fprintf(fp, "   velocity2com = %e\n",    velocity2com);
    fprintf(fp, "      accel2com = %e\n",       accel2com);
    fprintf(fp, "     energy2com = %e\n",      energy2com);
    fprintf(fp, "    senergy2com = %e\n",     senergy2com);
    fprintf(fp, "   pressure2com = %e\n",    pressure2com);
    fprintf(fp, "#####################################################################################\n");
    fprintf(fp, "Cosmological parameters in Computational Unit are tabulated below:\n");
    fprintf(fp, "  H_0    = %e\n", hubble);
    fprintf(fp, "rho_crit = %e\n", rho_crit);
    fprintf(fp, "#####################################################################################\n");

#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
    fclose(fp);
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
  }
}
