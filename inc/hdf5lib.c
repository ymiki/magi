/**
 * @file hdf5lib.c
 *
 * @brief Source code for original macros about HDF5
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <hdf5.h>

#include "macro.h"
#include "hdf5lib.h"


/**
 * @fn __chkHDF5err
 *
 * @brief wrapper for calling HDF5 functions
 *
 * @param (err) function call
 * @param (file) name of the file contains the called function
 * @param (line) line of the function in the specified file
 */
void __chkHDF5err(herr_t err, const char *file, const int line)
{
  /** Returns a non-negative value if successful; otherwise returns a negative value. */
  if( err < 0 ){
    fprintf(stderr, "Error is detected at %s(%d)\n", file, line);
    __KILL__(stderr, "HDF5 error, error ID is %d\n", err);
  }/* if( err < 0 ){ */
}
