/**
 * @file mpilib.h
 *
 * @brief Header file for utility packages to use MPI
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */
#ifndef MPILIB_H
#define MPILIB_H


#include <mpi.h>


/**
 * @struct MPIinfo
 *
 * @brief structure for MPI
 */
typedef struct
{
  int size, rank;
  MPI_Comm comm;
  MPI_Info info;
} MPIinfo;


#ifdef  DOUBLE_PRECISION
#define MPI_REALDAT MPI_DOUBLE
#else//DOUBLE_PRECISION
#define MPI_REALDAT MPI_FLOAT
#endif//DOUBLE_PRECISION

#ifndef MPI_BOOL
#define MPI_BOOL MPI_INT
#endif//MPI_BOOL


#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
/**
 * @def chkMPIerr(func)
 *
 * @brief wrapper to detect errors related to MPI
 */
#define chkMPIerr(err) __chkMPIerr(err, __FILE__, __LINE__)
#   if  ((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)
#pragma GCC diagnostic pop
#endif//((__GNUC_MINOR__ + __GNUC__ * 10) >= 45)


/* list of functions appeared in ``mpilib.c'' */
#ifdef  __CUDACC__
extern "C"
{
#endif//__CUDACC__
  void __chkMPIerr(int err, const char *file, const int line);

  void initMPI(MPIinfo *mpi, int *argc, char ***argv);
  void exitMPI(void);

  void setMPI(MPIinfo *mpi);
  void splitMPI(MPI_Comm old, int newGroupIdx, int newRankOrder, MPIinfo *newgroup);
  void freeMPIgroup(MPIinfo *old);

  void commitMPItype(MPI_Datatype *newtype, MPI_Datatype oldtype, int count);
  void commitMPIstruct(int num, int *blck, MPI_Aint *disp, MPI_Datatype *type, MPI_Datatype *newtype);
  void commitMPIbyte(MPI_Datatype *newtype, int size);
  void freeMPIstruct(MPI_Datatype *oldtype);
#ifdef  __CUDACC__
}
#endif//__CUDACC__


#endif//MPILIB_H
