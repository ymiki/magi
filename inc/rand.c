/**
 * @file rand.c
 *
 * @brief Source code for utility package to switch pseudo random number generators
 *
 * @author Yohei Miki (University of Tokyo)
 *
 * @date 2017/10/26 (Thu)
 *
 * Copyright (C) 2017 Yohei Miki
 * All rights reserved.
 *
 * The MIT License is applied to this software, see LICENSE.txt
 *
 */

#include <stdio.h>
#include <stdlib.h>

#ifdef  USE_SFMT
#include "SFMT.h"
#else///USE_SFMT
#include <gsl/gsl_rng.h>
#endif//USE_SFMT

#include "macro.h"
#include "rand.h"


/**
 * @fn initRandNum
 *
 * @brief Initialize the pseudo random numbers.
 *
 * @return (rand) state of the pseudo random number
 */
void initRandNum(rand_state **rand)
{
  __NOTE__("%s\n", "start");

#ifdef  USE_SFMT
  *rand = (rand_state *)malloc(sizeof(rand_state));
  if( *rand == NULL ){    __KILL__(stderr, "ERROR: failure to allocate *rand\n");  }
  /* sfmt_init_gen_rand(*rand,     5489);  /\**<     5489 for 32 bit Mersenne Twister *\/ */
  sfmt_init_gen_rand(*rand, 19650218);  /**< 19650218 for 64 bit Mersenne Twister */
#else///USE_SFMT
  const gsl_rng_type *RandType;
  gsl_rng_env_setup();
  RandType = gsl_rng_mt19937;
  *rand = gsl_rng_alloc(RandType);
  gsl_rng_set(*rand, 5489);
#endif//USE_SFMT

  __NOTE__("%s\n", "end");
}


/**
 * @fn freeRandNum
 *
 * @brief Finalize generating the pseudo random numbers.
 *
 * @param (rand) state of the pseudo random number
 */
void freeRandNum(rand_state  *rand)
{
  __NOTE__("%s\n", "start");

#ifdef  USE_SFMT
  free(rand);
#else///USE_SFMT
  gsl_rng_free(rand);
#endif//USE_SFMT

  __NOTE__("%s\n", "end");
}
